#include "os.h"

#ifdef Q_OS_WIN

#include <stdio.h>
#include "log.h"

#if 0
#include <stdafx.h>
#include "../monitor/RfReader.h"
#else
#include "RfReader.h"
#endif

#pragma execution_character_set ("utf-8")

#ifdef __cplusplus
extern "C" {
#endif
extern int Multi_Query_Tags_Epc(uint32_t times);
extern int Get_Multi_Query_Tags_Epc_Data(multi_query_epc_t &multi_epc);
extern int Stop_Multi_Query_Tags_Epc();
extern int Get_firmware_version(char *version);
extern int Get_Power(byte &loop, byte &read, byte &write);
extern int Set_Power(byte loop, byte read, byte write);
extern int Set_Work_Antanne(byte ants);
extern int Get_Work_Antanne(byte &ants);
extern int Set_antenna_worktime_and_waittime(ushort ant1_time, ushort ant2_time,
		ushort ant3_time, ushort ant4_time, ushort wait_time);
extern int Get_antenna_worktime_and_waittime(ushort &ant1_time, ushort &ant2_time,
		ushort &ant3_time, ushort &ant4_time, ushort &wait_time);
#ifdef __cplusplus
}
#endif

////////////////////////////////////////////////////////////////////////////////

void RfReader::dump_buf(byte *buf, int len)
{
    int i = 0;

	printf("---------- len = %d ----------------------", len);
	for (i = 0; i < len; i++) {
		if (i % 16 == 0)
			printf("\n");
		printf("0x%02x ", buf[i]);
	}
	printf("\n--------------------------------\n");
}

void RfReader::initCmd(byte *cmd, int len, byte type)
{
	cmd[0] = 0xbb;     // head
	cmd[1] = type;     // type
	cmd[2] = len - 6;  // data len
	cmd[len - 2] = 0x0d; // end1
	cmd[len - 1] = 0x0a; // end2
}

void RfReader::doCrc(byte *cmd, int len)
{
	for (int i = 1; i < len - 3; i++)
		cmd[len - 3] += cmd[i];
}

int32_t RfReader::recv_resp(uint8_t *resp, int len)
{
	int32_t ret;
	int32_t index = 0;

	do {
		if (index >= len) {
			return 0;
		}
		ret = transfer_recv(resp + index);
		if (ret > 0)
			index += ret;
		else if (ret <= 0)
			break;
	} while (1);

	return index;
}

////////////////////////////////////////////////////////////////////////////////

RfReader::RfReader()
{
	m_isMultiQueryStart = false;
	m_netDevice = NULL;
}

RfReader::~RfReader()
{
	if (m_isMultiQueryStart) {
		StopMultiQuery_v2();
	}
	DeinitTransfer();
}

bool RfReader::InitTransfer()
{
	int32_t ret;

	m_initOk = false;

	ret = basic_init(CONNECT_NET);
	if (ret != 0) {
		errLog(QString("�Ķ��� basic init ʧ��, ret = %1").arg(ret));
		return false;
	}

	memset(&m_netDevices, 0, sizeof(m_netDevices));
	ret = transfer_init(&m_netDevices);
	if (ret != 0) {
		errLog(QString("�Ķ��� transfer init ʧ��, ret = %1").arg(ret));
		return false;
	}
	if (m_netDevices.device_num <= 0) {
		errLog("δ�ҵ��Ķ����豸");
		return false;
	}
 
	// use the first net device
	m_netDevice = &m_netDevices.net_device[0];
	ret = transfer_open(m_netDevice);
	if (ret	!= 0) {
		if (ret == NET_ERROR_SOCKET_INIT)
			errLog("�Ķ���PC�˴���Socketʧ��");
		else if (ret == NET_ERROR_SOCKET_ACCEPT)
			errLog("�Ķ���PC��accept�ͻ���ʧ��");
		else if (ret == NET_ERROR_SOCKET_CONNECT)
			errLog("�Ķ���PC�ͻ�������ʧ��\n");
		else
			errLog(QString("�Ķ���δ֪����, ret = %1").arg(ret));
		return false;
	}
	transfer_recv_set(NULL);

	m_initOk = true;
	infoLog(QString("�Ķ���IPΪ: %1").arg(m_netDevice->device_info.strIP));
	return true;
}
	
void RfReader::DeinitTransfer()
{
	transfer_close(NULL);
}
	
void RfReader::PrintNetDevice(device_t *dev)
{
	DEVICEINFO *info = &dev->device_info;
	C2000NETSETTING *netSet = &dev->net_setting;
	C2000SOCKETSETTING *skSet = &dev->socket_setting;

	printf("device info:\n");
	printf("  Name = %s, MAC: %s, IP: %s, Model: %d, Version: %d\n",
			info->strName, info->strMAC, info->strIP, info->dwModel, info->dwVersion);

	printf("net setting:\n");
	printf("  index: %d, staticIP: %s, IP: %s, netmask: %s, Gateway: %s\n",
			netSet->nNetIndex, netSet->bUseStaticIP ? "static": "DHCP",
			netSet->ipAddr, netSet->ipNetMask, netSet->ipGateway);
	printf("  DnsServer: %s\n", netSet->ipDnsServer);

	printf("socket setting:\n");
	printf("  index: %d, mode: %d, Local Port: %d, PeerName: %s, PeerPort: %d, use socket:%s\n",
			skSet->nSocketIndex, skSet->nWorkMode, skSet->nLocalPort, skSet->szPeerName,
			skSet->nPeerPort, skSet->bUseSocket ? "yes" : "no");
}

////////////////////////////////////////////////////////////////////////////////
	
bool RfReader::GetFirmwareVersion(char *version, size_t len)
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	char ver[64] = {0};
	int ret = Get_firmware_version(ver);

	if (ret != 0) {
		errLog("�Ķ�����ȡ�̼��汾ʧ��");
		return false;
	}

	strcpy_s(version, len, ver);
	return true;
}

bool RfReader::GetPower(uint8_t &read_power, uint8_t &write_power)
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	byte loop, read, write;
	int ret = Get_Power(loop, read, write);
	if (ret != 0) {
		return false;
	}
	read_power = read;
	write_power = write;
	return true;
}

bool RfReader::SetPower(uint8_t read_power, uint8_t write_power)
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	byte loop = 0;
	int ret = Set_Power(loop, read_power, write_power);
	if (ret != 0) {
		return false;
	}
	return true;
}

bool RfReader::GetCurrentAntanne(byte &number)
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	int ret = Get_Work_Antanne(number);
	if (ret != 0) {
		printf("Get_Work_Antanne failed, ret = %d\n", ret);
		return false;
	}

	return true;
}

bool RfReader::SetCurrentAntanne(byte number)
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	int ret = Set_Work_Antanne(number);
	if (ret != 0) {
		printf("Set_Work_Antanne failed, ret = %d\n", ret);
		return false;
	}

	return true;
}

bool RfReader::StartMultiQuery_v2()
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}

	int ret = Multi_Query_Tags_Epc(0);
	if (ret != 0) {
		printf("Start Multi query failed, ret = %d.\n", ret);
		return false;
	}


	m_isMultiQueryStart = true;
	return true;
}
	
bool RfReader::ReadMultiQuery_v2(tag_t *tag)
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	multi_query_epc_t epcs;
	int ret;
	
	ret = Get_Multi_Query_Tags_Epc_Data(epcs);
	if (ret != 0) {
		printf("Get_Multi_Query_Tags_Epc_Data failed, ret = %d\n", ret);
		return false;
	}

	for (int index = 0; index < epcs.packet_num; index++ ) {
		query_epc_t *in_tag = &epcs.tags_epc[index];
		epc_t *epc = &in_tag->epc;

		if ((epc->epc_len > 124) || (epc->epc_len <= 0)) {
			continue;
		}
		memcpy(tag->epc, epc->epc, EPC_LEN);
		tag->antanne_id = in_tag->ant_id;
		int16_t rssi = -(~(in_tag->rssi_msb << 8 | in_tag->rssi_lsb) + 1);
		tag->rssi = (double)rssi / 10;
	}

	return true;
}

bool RfReader::StopMultiQuery_v2()
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	int ret = Stop_Multi_Query_Tags_Epc();

	if (0 != ret) {
		printf("Stop Multi query fail: ret = %d.\n", ret);
		return false;
	}

	m_isMultiQueryStart = false;
	return true;
}

bool RfReader::GetWorkTime(ushort &t1, ushort &t2, ushort &t3, ushort &t4, ushort &wt)
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	int ret = Get_antenna_worktime_and_waittime(t1, t2, t3, t4, wt);
	if (ret != 0) {
		printf("get work time failed: %d\n", ret);
		return false;
	}

	return true;
}

bool RfReader::SetWorkTime(ushort t1, ushort t2, ushort t3, ushort t4, ushort wt)
{
	if (!m_initOk) {
		warLog(QString("�Ķ�����ʼ���д����޷�ִ�иò��� %1.").arg(__FUNCTION__));
		return false;
	}
	int ret = Set_antenna_worktime_and_waittime(t1, t2, t3, t4, wt);
	if (ret != 0) {
		printf("set work time failed: %d\n", ret);
		return false;
	}

	return true;
}

#endif
