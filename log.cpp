#include "log.h"
#include <qcolor.h>
#include <qmutex.h>
#include <qdebug.h>
#include <qdatetime.h>
#include <iostream>
#include "os.h"
#include <qthread.h>

static QListWidget *logWidget;
static QMutex mutex;

static bool debug = true;

void initLog(QListWidget *w)
{
	logWidget = w;
}

static void addLog(QString str, QColor color)
{
	mutex.lock();
	logWidget->addItem(str);
	logWidget->item(logWidget->count() - 1)->setBackgroundColor(color);
	mutex.unlock();
}

void errLog(QString str)
{
	addLog(str, Qt::red);
}

void okLog(QString str)
{
	addLog(str, Qt::green);
}

void warLog(QString str)
{
	addLog(str, Qt::yellow);
}

void infoLog(QString str)
{
	addLog(str, Qt::white);
}

void logD(QString str)
{
	if (debug) {
		std::cout << "[" << QTime::currentTime().toString("hh:mm:ss.zzz").toLatin1().data() << "] "
			<< str.toLatin1().data() << std::endl;
	}
}

////////////////////////////////////////////////////////////////////////////////

#ifdef Q_OS_WIN
void setup_console()
{
	AllocConsole();
	freopen("CONOUT$", "w+t", stdout);
	//freopen("CONOUT$", "w+t", stderr);
	freopen("CONIN$", "r+t", stdin);
}

void release_console()
{
    FreeConsole();
}

#endif
