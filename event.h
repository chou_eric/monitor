#pragma once

#include <QtCore/QTextStream>
#include <QtCore/QFile>
#include <QtCore/QIODevice>

class tagFile {
public:
	tagFile(QString filename);
	~tagFile();
	
	// add events with timestamp
	void addStartEvent();
	void addTagEvent(char *tag);
	void addStopEvent();

private:
	QFile m_file;

private:
	void formatEventHeader(char *str);
	void addEvent(QString str);


};
