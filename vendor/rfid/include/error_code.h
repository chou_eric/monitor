/**
  ******************************************************************************
  * @file      error_code.h
  * @author    sanray
  * @version   V3.0.0
  * @date      2013.08.23
  * @brief     错误码
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ERROR_CODE_H__
#define __ERROR_CODE_H__

/* Include -------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif
/** @addtogroup Drivers
  * @{
  */
/** @addtogroup Doxygen_Demo
  * @{
  */



/**
  ******************************************************************************
  * @defgroup Doxygen_Demo_Configure
  * @brief 用户配置
  ******************************************************************************
  * @{
  */
//#define __EZOS_TASK_NUM 16 ///< 设置最大任务数
/**
  * @}
  */



/** @defgroup Doxygen_Demo_Public_TypeDefine
  * @brief 公有类型定义
  * @{
  */

/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_MacroDefine
  * @brief 公有宏定义
  * @{
  */

// 魔幻数
#define	MAGIC_NUMBER	1000

// 命令不匹配
#define	ERROR_COMMAND_NOT_MATCH			(MAGIC_NUMBER - 1)
// 命令打包出错
#define	ERROR_COMMAND_PACKET			(MAGIC_NUMBER - 2)
// 没有命令返回
#define	ERROR_COMMAND_NOT_RESPONSE		(MAGIC_NUMBER - 3)
// 接收到的命令不完整
#define	ERROR_COMMAND_RECV_INCOMPLETE	(MAGIC_NUMBER - 4)
// 错误响应
#define	ERROR_COMMAND_RESPONSE			(MAGIC_NUMBER - 5)
// 模块对该命令没有成功操作
#define	ERROR_COMMAND_OPER_FAIL			(MAGIC_NUMBER - 6)
// 初始化连接方式错误
#define	ERROR_CONNECT_TYPE				(MAGIC_NUMBER - 7)
// 接收到的数据长度过长（可能会导致溢出，默认接收的一帧的数据的长度为 PACKET_MID）
#define	ERROR_COMMAND_RECV_DATA_FILL	(MAGIC_NUMBER - 8)
// 接收到的数据异常
#define	ERROR_COMMAND_RECV_DATA_ERROR	(MAGIC_NUMBER - 9)



/************************************************************************/
/* 传输实现                                                              */
/************************************************************************/
//没有实现 init 函数，init 函数指针为空NULL
#define	ERROR_INIT_Function_Not_Implement		(MAGIC_NUMBER + 1)
//没有实现 open 函数，open 函数指针为空NULL
#define	ERROR_OPEN_Function_Not_Implement		(MAGIC_NUMBER + 2)
	//没有实现 send 函数，send 函数指针为空NULL
#define	ERROR_CLOSE_Function_Not_Implement		(MAGIC_NUMBER + 3)

//没有实现 send 函数，send 函数指针为空NULL
#define	ERROR_SEND_Function_Not_Implement		(MAGIC_NUMBER + 4)
//没有实现 send_set 函数，send_set 函数指针为空NULL
#define	ERROR_SEND_SET_Function_Not_Implement	(MAGIC_NUMBER + 5)

//没有实现 recv 函数，recv 函数指针为空NULL
#define	ERROR_RECV_Function_Not_Implement		(MAGIC_NUMBER + 6)
//没有实现 recv_set 函数，recv_set 函数指针为空NULL
#define	ERROR_RECV_SET_Function_Not_Implement	(MAGIC_NUMBER + 7)


//功率
#define	ERROR_POWER_LOOP						(MAGIC_NUMBER + 8)		// 开闭环错误
#define	ERROR_PWOER_READ_INVALID				(MAGIC_NUMBER + 9)		// 读功率错误
#define	ERROR_PWOER_WRITE_INVALID				(MAGIC_NUMBER + 10)		// 写功率错误

// 天线
#define	ERROR_ANTENNA_NONE						(MAGIC_NUMBER + 11)		// 没有设定当前工作天线

// 单次查询标签EPC
// 电磁波反射过大,The reflection of electromagnetic wave is too large
#define	ERROR_SINGLE_QUERY_EPC_Large_Reflection	(MAGIC_NUMBER + 12)

// 循环寻标签
// pc值计算错误
#define	ERROR_MULTI_PC_COUNT					(MAGIC_NUMBER + 13)		// 根据接收到的数据转换pc值出错
#define	ERROR_MULTI_CURRENT_RECV_FINISH			(MAGIC_NUMBER + 14)		// 当前接收的数据中已没有所需的数据包
#define	ERROR_MULTI_STOP_SUCCESS				(MAGIC_NUMBER + 15)		// 成功退出循环寻标签
#define	ERROR_MULTI_INVALID_PACKET				(MAGIC_NUMBER + 16)		// 当前接收的数据缓冲区中的没有所需的数据包
#define	ERROR_MULTI_INCOMPLETE_PACKET			(MAGIC_NUMBER + 17)		// 缓冲区的数据无法组成一个完整的所需的数据包
#define	ERROR_MULTI_EMPTY_PACKET				(MAGIC_NUMBER + 18)		// 数据包为空，缓冲区的数据已处理完，没有数据组成数据包
#define	ERROR_MULTI_PACKET_DATA					(MAGIC_NUMBER + 19)		// 数据包的数据有异常
#define	ERROR_MULTI_RECV_OVER_MAX				(MAGIC_NUMBER + 20)		// 循环寻标签时每次接收的数据过大

#define	ERROR_TAG_FILTER_MB						(MAGIC_NUMBER + 21)		// 读写操作时的过滤内存区异常（非0，非1）
#define	ERROR_DATA_TO_LARGE_TO_PACKET			(MAGIC_NUMBER + 22)		// 数据过长，无法构成一个合法的数据包

#define	ERROR_PASSWORD_IS_EMPTY					(MAGIC_NUMBER + 23)		// 密码不可为空
#define	ERROR_FILTER_IS_EMPTY					(MAGIC_NUMBER + 24)		// 过滤数据为空

//gen2_param
#define	ERROR_Q_DATA_INAVLID					(MAGIC_NUMBER + 25)		// Q_DATA数据无效
#define	ERROR_START_Q_DATA_INAVLID				(MAGIC_NUMBER + 26)		// start_q数据无效
#define	ERROR_MIN_Q_INAVLID						(MAGIC_NUMBER + 27)		// min_q数据无效
#define	ERROR_MAX_Q_DATA_INAVLID				(MAGIC_NUMBER + 28)		// max_q数据无效
//20150413
#define	ERROR_SELECT_Q_DATA_INAVLID				(MAGIC_NUMBER + 29)		// select_q数据无效
#define	ERROR_SESSION_Q_DATA_INAVLID			(MAGIC_NUMBER + 30)		// session_q数据无效
#define	ERROR_TARGET_Q_DATA_INAVLID				(MAGIC_NUMBER + 31)		// target_q数据无效


/**	
  * @}
  */

/** @defgroup Doxygen_Demo_Public_Variable
  * @brief 声明公有全局变量
  * @{
  */

/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_Function
  * @brief 定义公有函数
  * @{
  */ 
 
/**
  * @}
  */



/**
  * @}
  */
/**
  * @}
  */
#ifdef __cplusplus
}
#endif

#endif
/* END OF FILE ---------------------------------------------------------------*/

