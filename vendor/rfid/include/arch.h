/**
  ******************************************************************************
  * @file      arch.h
  * @author    sanray
  * @version   V3.0.0
  * @date      2013.08.23
  * @brief     体系结构
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ARCH_H__
#define __ARCH_H__

/* Include -------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif
/** @addtogroup Drivers
  * @{
  */
/** @addtogroup Doxygen_Demo
  * @{
  */



/**
  ******************************************************************************
  * @defgroup Doxygen_Demo_Configure
  * @brief 用户配置
  ******************************************************************************
  * @{
  */
//#define __EZOS_TASK_NUM 16 ///< 设置最大任务数
/**
  * @}
  */



/** @defgroup Doxygen_Demo_Public_TypeDefine
  * @brief 公有类型定义
  * @{
  */

/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_MacroDefine
  * @brief 公有宏定义
  * @{
  */

//1大小端

// 单通道，还是四通道
#define	FOUR_CHANNELS

// 当前版本
#define	CURRENT_VERSION		114

/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_Variable
  * @brief 声明公有全局变量
  * @{
  */

/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_Function
  * @brief 定义公有函数
  * @{
  */ 
 
/**
  * @}
  */



/**
  * @}
  */
/**
  * @}
  */
#ifdef __cplusplus
}
#endif

#endif
/* END OF FILE ---------------------------------------------------------------*/
