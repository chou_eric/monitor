/**
  ******************************************************************************
  * @file      types.h
  * @author    sanray
  * @version   V3.0.0
  * @date      2013.08.23
  * @brief     类型定义
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TYPES_H__
#define __TYPES_H__

/* Include -------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arch.h"
#include "error_code.h"
//#include "sr_api.h"

#ifdef __cplusplus
extern "C" {
#endif
/** @addtogroup Drivers
  * @{
  */
/** @addtogroup Doxygen_Demo
  * @{
  */



/**
  ******************************************************************************
  * @defgroup Doxygen_Demo_Configure
  * @brief 用户配置
  ******************************************************************************
  * @{
  */
//#define __EZOS_TASK_NUM 16 ///< 设置最大任务数
/**
  * @}
  */



	/** @defgroup Doxygen_Demo_Public_MacroDefine
  * @brief 公有宏定义
  * @{
  */

//操作正常
#define	OPER_OK							(0)

// 模块对命令的操作的成功，失败
#define	COMMAND_OPER_SUCCESS			(0x01)
#define	COMMAND_OPER_FAIL				(0x00)
//命令包长度
#define	RECV_PACKET_NUM					50			// 循环寻标签时，接收一次，最多包含的完整的返回命令包的个数
#define	PACKET_128						128
#define	PACKET_MIN						32

#ifdef	FOUR_CHANNELS
#define	PACKET_SIZE						23			// 有天线号
#else
#define	PACKET_SIZE						22
#endif

#if (RECV_PACKET_NUM > 10)
#define	PACKET_MID						(RECV_PACKET_NUM*PACKET_SIZE)
#else
#define PACKET_MID						256
#endif

#define	PACKET_MAX						(PACKET_MID*2)

// 密码的字节长度
#define	PASSWORD_LEN					4

// EPC 的长度
#define	EPC_SIZE_MIN					0
#define	EPC_SIZE_MAX					62

//QT操作
#define READ_FLAG						1
#define WRITE_FLAG						2
/**
  * @}
  */



/** @defgroup Doxygen_Demo_Public_TypeDefine
  * @brief 公有类型定义
  * @{
  */
typedef    char                   int8_t;      /**<  8bit integer type */
typedef    short                  int16_t;     /**< 16bit integer type */
typedef    int                    int32_t;     /**< 32bit integer type */
typedef unsigned char                   uint8_t;     /**<  8bit unsigned integer type */
typedef unsigned short                  uint16_t;    /**< 16bit unsigned integer type */
typedef unsigned int                    uint32_t;    /**< 32bit unsigned integer type */

//通信接口
typedef struct _transfer 
{
	int32_t (*init)(void * param);								// 初始化
	int32_t	(*open)(void * param);								// 打开通讯
	int32_t	(*close)(void * param);								// 关闭通讯
	int32_t	(*send)(uint8_t *send_buffer,int32_t data_len);		// 发送
	int32_t (*send_set)(void * param);							// 发送设置
	int32_t	(*recv)(uint8_t * recv_buffer);						// 接收
	int32_t (*recv_set)(void * param);							// 接收设置
}transfer_t;

//命令类型
enum	command_type
{
	/* command *****************************************************************************/
	SET_POWER	= 0x00,						// 0x00 设置功率
	SET_GPIO_LEVEL,							// 0x01 设置 GPIO 电平
	SET_OUTPUT_FREQUENCY,					// 0x02 设置射频输出频率
	SET_RF_LINK,							// 0x03 设置 RF LINK PROFILE
	SET_REGISTER,							// 0x04 设置寄存器
	SET_ANTENNA_CARRIER,					// 0x05 设置天线载波
	GET_REGISTER_DATA,						// 0x06 查询寄存器数据
	SET_GEN2_PARAM,							// 0x07 设置 gen2 参数
	SET_WORK_ANTANNE,						// 0x08 设置当前工作天线
	SET_FREQUENCY_REGION,					// 0x09 设置读写器频率区域
	GET_HARDWARE_VERSION,					// 0x0A 查询 Hardware（硬件）版本号 
	GET_FIRMWARE_VERSION,					// 0x0B 查询 Firmware（固件）版本号
	GET_POWER,								// 0x0C 查询功率
	GET_FREQUENCY_STATE,					// 0x0D 查询射频频率状态
	GET_RF_LINK,							// 0x0E	查询 RF LINK PROFILE 参数设置
	GET_ANTENNA_CARRIER,					// 0x0F 查询天线载波设置
	GET_WORK_ANTANNE,						// 0x10 查询当前工作天线设置
	GET_FREQUENCY_REGION,					// 0x11 查询读写器频率区域
	GET_MODULE_TEMPERATURE,					// 0x12 查询读写器模块当前温度
	GET_GPIO_LEVEL,							// 0x13 查询 GPIO 电平状态
	GET_GEN2_PARAM,							// 0x14 查询 gen2 参数设置
	SET_FIRMWARE_UPGRADE_ONLINE,			// 0x15 固件在线升级设置
	SINGLE_QUERY_TAGS_EPC,					// 0x16 单次查询标签 EPC
	MULTI_QUERY_TAGS_EPC,					// 0x17 循环查询标签 EPC
	STOP_MULTI_QUERY_TAGS_EPC,				// 0x18 停止循环查询标签 EPC
	READ_TAGS_DATA,							// 0x19 查询标签数据
	WRITE_TAGS_DATA,						// 0x1A 写入标签数据
	LOCK_TAGS,								// 0x1B 锁定标签
	KILL_TAGS,								// 0x1C Kill标签
	SET_MULTI_QUERY_TAGS_INTERVAL,			// 0x1D 设置循环查询标签周期间断时间 
	GET_MULTI_QUERY_TAGS_INTERVAL,			// 0x1E 查询循环查询标签周期间断时间设置 
	SET_ANTENNA_WORKTIME_AND_WAITTIME,		// 0x1F 设置天线工作时间及等待时间 
	GET_ANTENNA_WORKTIME_AND_WAITTIME,		// 0x20 获取天线工作时间及等待时间 
	SET_FASTID,								// 0x21 设置FastID 
	GET_FASTID,								// 0x22 获取FastID设置
	SET_MODULE_BAUD_RATE,					// 0x23 设置模块通讯波特率
	

	// 自定义
	WRITE_TAGS_EPC,							// 写标签的EPC
	GET_MULTI_QUERY_TAGS_EPC,				// 接收循环寻标签的数据
	SET_QT_PARAMS = 0x26,					// 0x26 设置QT参数
	GET_QT_PARAMS,							// 0x27 获取qt参数
	QT_PEK_OPERATING,						// 0x28 带QT操作标签
	SET_TAGFOCUS,                           //0x29  设置tagFocus 参数 
	GET_TAGFOCUS,                           //0X2A  获取tagFocus设置  

	COMMAND_MAX,							//命令总数

	/* command respond *********************************************************************/
	SET_POWER_RESPOND		= 0x80,			// 0x80 设置功率 响应
	SET_GPIO_LEVEL_RESPOND,					// 0x81 设置 GPIO 电平 响应
	SET_OUTPUT_FREQUENCY_RESPOND,			// 0x82 设置射频输出频率 响应
	SET_RF_LINK_RESPOND,					// 0x83 设置 RF LINK PROFILE 响应
	SET_REGISTER_RESPOND,					// 0x84 设置寄存器 响应
	SET_ANTENNA_CARRIER_RESPOND,			// 0x85 设置天线载波 响应
	GET_REGISTER_DATA_RESPOND,				// 0x86 查询寄存器数据 响应
	SET_GEN2_PARAM_RESPOND,					// 0x87 设置 gen2 参数 响应
	SET_WORK_ANTANNE_RESPOND,				// 0x88 设置当前工作天线 响应
	SET_FREQUENCY_REGION_RESPOND,			// 0x89 设置读写器频率区域 响应
	GET_HARDWARE_VERSION_RESPOND,			// 0x8A 查询 Hardware（硬件）版本号 响应
	GET_FIRMWARE_VERSION_RESPOND,			// 0x8B 查询 Firmware（固件）版本号 响应
	GET_POWER_RESPOND,						// 0x8C 查询功率 响应
	GET_FREQUENCY_STATE_RESPOND,			// 0x8D 查询射频频率状态 响应
	GET_RF_LINK_RESPOND,					// 0x8E	查询 RF LINK PROFILE 参数设置 响应
	GET_ANTENNA_CARRIER_RESPOND,			// 0x8F 查询天线载波设置 响应
	GET_WORK_ANTANNE_RESPOND,				// 0x90 查询当前工作天线设置 响应
	GET_FREQUENCY_REGION_RESPOND,			// 0x91 查询读写器频率区域 响应
	GET_MODULE_TEMPERATURE_RESPOND,			// 0x92 查询读写器模块当前温度 响应
	GET_GPIO_LEVEL_RESPOND,					// 0x93 查询 GPIO 电平状态 响应
	GET_GEN2_PARAM_RESPOND,					// 0x94 查询 gen2 参数设置 响应
	SET_FIRMWARE_UPGRADE_ONLINE_RESPOND,	// 0x95 固件在线升级设置 响应
	SINGLE_QUERY_TAGS_EPC_RESPOND,			// 0x96 单次查询标签 EPC 响应
	MULTI_QUERY_TAGS_EPC_RESPOND,			// 0x97 循环查询标签 EPC 响应
	STOP_MULTI_QUERY_TAGS_EPC_RESPOND,		// 0x98 停止循环查询标签 EPC 响应
	READ_TAGS_DATA_RESPOND,					// 0x99 查询标签数据 响应
	WRITE_TAGS_DATA_RESPOND,				// 0x9A 写入标签数据 响应
	LOCK_TAGS_RESPOND,						// 0x9B 锁定标签 响应
	KILL_TAGS_RESPOND,						// 0x9C Kill标签 响应
	SET_MULTI_QUERY_TAGS_INTERVAL_RESPOND,	// 0x9D 设置循环查询标签周期间断时间 响应
	GET_MULTI_QUERY_TAGS_INTERVAL_RESPOND,	// 0x9E 查询循环查询标签周期间断时间设置 响应 
	SET_ANTENNA_WORKTIME_AND_WAITTIME_RESPOND,		// 0x9F 设置天线工作时间及等待时间 响应
	GET_ANTENNA_WORKTIME_AND_WAITTIME_RESPOND,		// 0xA0 获取天线工作时间及等待时间 响应
	SET_FASTID_RESPOND,								// 0xA1 设置FastID 响应 
	GET_FASTID_RESPOND,								// 0xA2 获取FastID设置 响应
	SET_MODULE_BAUD_RATE_RESPOND,					// 0xA3 设置模块通讯波特率 响应
	SET_QT_PARAMS_RESPOND = 0xA6,					// 0xA6 获取QT参数响应
	GET_QT_PARAMS_RESPOND = 0xA7,					// 0xA7	设置QT参数响应
	QT_PEK_OPERATING_RESPOND = 0xA8,				// 0xA8 带QT操作标签响应
    SET_TAGFOCUS_RESPOND,                           //0xA9  设置tagFocus 参数响应 
	GET_TAGFOCUS_RESPOND,

	COMMAND_ERROR_RESPOND	= 0xFF,			// 0xFF 错误命令码					
};




#pragma pack(push)    //保存对齐状态
#pragma pack(1)       //设定为1字节对齐

// 命令头
#define	COMMAND_HEAD				(0xBB)
#define	COMMAND_HEAD_TYPE_OFFSET	(0x01)
typedef	struct _command_head
{
	uint8_t		head;
	uint8_t		type;
	uint8_t		len;

	_command_head()
	{
		head	= COMMAND_HEAD;
		type	= 0x00;
		len		= 0x00;
	}
}command_head_t;

// 命令尾
#define	COMMAND_TAIL_D		(0x0D)
#define	COMMAND_TAIL_A		(0x0A)
typedef struct _command_tail
{
	uint8_t		crc;
	uint8_t		end_d;
	uint8_t		end_a;

	_command_tail()
	{
		crc		= 0;
		end_d	= COMMAND_TAIL_D;
		end_a	= COMMAND_TAIL_A;
	}
}command_tail_t;


// 功率 （设置，获取）
typedef struct _power
{
	uint8_t		com_type;			// 命令类型（0x00,0x80;0x0C,0x8C）
	uint8_t		loop;				// 开闭环（开环（默认状态）：0x00;闭环：0x01）
	uint8_t		read;				// 读功率（取值范围：5~30 dBm）
	uint8_t		write;				// 写功率（取值范围：5~30 dBm）
}power_t;
#define	LOOP_OPEN			(0x00)
#define LOOP_CLOSE			(0x01)



// 天线 （设置，获取）
typedef struct _antenna
{
	uint8_t		com_type;			// 命令类型（0x08,0x88;0x10,0x90）
	uint8_t		ants;				// 天线号（bit0：ant1；bit1：ant2；... bit7：ant8）
}antenna_t;


/************************************************************************
一、GNU 风格的版本号命名格式 :
主版本号 . 子版本号 [. 修正版本号 [. 编译版本号 ]]
Major_Version_Number.Minor_Version_Number[.Revision_Number[.Build_Number]]
示例 : 1.2.1, 2.0, 5.0.0 build-13124

二、Windows 风格的版本号命名格式 :
主版本号 . 子版本号 [ 修正版本号 [. 编译版本号 ]]
Major_Version_Number.Minor_Version_Number[Revision_Number[.Build_Number]]
示例: 1.21, 2.0
************************************************************************/
// 固件版本号，硬件版本号
typedef struct _ware
{
	uint8_t		com_type;			// 命令类型（0x0A,0x8A;0x0B,0x8B）
	uint8_t		major_version;		// 主版本号
	uint8_t		minor_version;		// 子版本号
	uint8_t		revision_version;	// 修正版本号
}ware_t;


/************************************************************************
温度×100，转换为十六进制后，负数则取补码
例子：当前模块温度为 -40℃，-40*100 = -4000 = 0xFO60,
		则 temp_msb = 0xF0;temp_lsb = 0x60
************************************************************************/
// 模块的温度
typedef	struct _temperature
{
	uint8_t		com_type;			// 命令类型（0x12,0x92）
	uint8_t		temp_msb;			// 温度值的高8位
	uint8_t		temp_lsb;			// 温度值的低8位
}temperature_t;



/************************************************************************
循环查询标签周期间断时间，为0 时表示循环不间断寻卡，最大为
0xFFFF，单位是 ms
************************************************************************/
// 循环查询标签周期间断时间
typedef	struct _multi_interval
{
	uint8_t		com_type;			// 命令类型（0x1D,0x9D;0x1E,0x9E）
	uint8_t		work_time_msb;		// 循环查询标签周期工作时间 高8位
	uint8_t		work_time_lsb;		// 循环查询标签周期工作时间 低8位
	uint8_t		interval_msb;		// 时间间隔值的高8位
	uint8_t		interval_lsb;		// 时间间隔值的低8位
}multi_interval_t;



/************************************************************************
说明：设置天线工作时间只适用四端口模块，Data0，Data1为天线1的工作
时间（单位ms，范围30ms—60000ms），Data2，Data3为天线2的工作时间（单
位ms，范围30ms—60000ms），Data4，Data5为天线3的工作时间（单位ms，
范围30ms—60000ms），Data6，Data7为天线4 的工作时间（单位ms，范围
30ms—60000ms），Data8，Data9为等待时间（单位ms，范围0ms—60000ms），

如果是使用部分天线，则只需使能需要的天线号就可以了

例：设置天线1的工作时间为100ms，天线2工作时间150ms，天线3工作
时间314ms，天线4工作时间30ms，等待时间10000ms。
命令：BB 1F 0A 00 64 00 96 01 3A 00 1E 27 10 B3 0D 0A
************************************************************************/
// 天线工作时间及等待时间
typedef	struct	_antenna_time
{
	uint8_t		com_type;			// 命令类型（0x1F,0x9F;0x20,0xA0）
	uint8_t		ant1_msb;			// 天线1的工作时间值的高8位
	uint8_t		ant1_lsb;			// 天线1的工作时间值的低8位
	uint8_t		ant2_msb;			// 天线2的工作时间值的高8位
	uint8_t		ant2_lsb;			// 天线2的工作时间值的低8位
	uint8_t		ant3_msb;			// 天线3的工作时间值的高8位
	uint8_t		ant3_lsb;			// 天线3的工作时间值的低8位
	uint8_t		ant4_msb;			// 天线4的工作时间值的高8位
	uint8_t		ant4_lsb;			// 天线4的工作时间值的低8位
	uint8_t		wait_msb;			// 等待时间值的高8位
	uint8_t		wait_lsb;			// 等待时间值的低8位
}antenna_time_t;


typedef	struct	_epc
{
	uint8_t		pc_msb;
	uint8_t		pc_lsb;
	int8_t		epc_len;
	uint8_t		epc[PACKET_128];	
}epc_t;


/************************************************************************
说明：RSSI以补码的形式表示，共16bit，为实际值×10。如-65.7dBm，则
RSSI=FD6F = -657;
************************************************************************/
// 查询标签EPC 
typedef	struct _query_epc
{
	uint8_t		com_type;			// 命令类型（0x16,0x96）
#if 0
	uint8_t		pc_msb;
	uint8_t		pc_lsb;
	int32_t		epc_len;
	uint8_t		epc[PACKET_MID];
#else
	epc_t		epc;
#endif
	uint8_t		rssi_msb;
	uint8_t		rssi_lsb;
#ifdef FOUR_CHANNELS
	uint8_t		ant_id;				// 天线号
#endif

	int32_t	tid_len;
	uint8_t		tid[PACKET_128];

}query_epc_t;


/************************************************************************
说明：循环查询标签EPC次数范围为1~0xFFFF，为0时，表示永久查询标
签EPC 
例：循环查询标签EPC次数为100次
命令：BB 17 02 00 64 7D 0D 0A 
************************************************************************/
typedef	struct	_multi_query_epc
{
	uint8_t		com_type;			// 命令类型（0x17,0x97;0x18,0x98）
	uint8_t		query_total_msb;	// 查询次数的高8位
	uint8_t		query_total_lsb;	// 查询次数的低8位
	uint8_t		packet_num;			// 完整的数据包的个数
	query_epc_t	tags_epc[RECV_PACKET_NUM];	// 标签的EPC相关信息
}multi_query_epc_t;

typedef	struct	_tid
{
	int8_t		tid_len;
	uint8_t		tid[PACKET_128];	
}tid_t;

/************************************************************************
说明：AP为标签的访问密码；PC+EPC过滤查询需要，若不过滤，则必须
全部置零；MB为用户需要查询的数据的bank号；SA为需查询的数据的起始地
址，单位为字；DL为需查询的数据长度，单位为字
************************************************************************/
// 读取，写入标签数据
#define	FILTER_EPC						0x00
#define	FILTER_TID						0x01
// 读取，写入标签数据
typedef	struct	_tags_data
{
	uint8_t		com_type;				// 命令类型（0x19,0x99;0x1A,0x9A）
	uint8_t		password[PASSWORD_LEN];	// 密码
	uint8_t		filter_mem_bank;		// 要过滤的标签的内存区.0为epc区，1为tid区
	uint8_t		filter_data_len_msb;	
	uint8_t		filter_data_len_lsb;
	epc_t		epc;
	tid_t		tid;
	uint8_t		mem_bank;				// 要读写的标签的内存区
	uint8_t		start_addr_msb;			// 要读写的标签的内存区的开始位置的高8位
	uint8_t		start_addr_lsb;			// 要读写的标签的内存区的开始位置的低8位
	uint8_t		data_len_msb;			// 要读写的数据长度的高8位，单位为字
	uint8_t		data_len_lsb;			// 要读写的数据长度的低8位，单位为字
#ifdef FOUR_CHANNELS
	uint8_t		ant_id;					// 天线号
#endif
	uint8_t		data[PACKET_MAX];		// 要读写的数据
}tags_data_t;


// 写标签的EPC
typedef	struct	_tags_epc
{
	uint8_t		com_type;				// 命令类型 WRITE_TAGS_EPC
	uint8_t		epc[PACKET_128];		
	uint8_t		epc_len;
}tags_epc_t;


// 模块外接的GPIO
typedef	struct	_gpio
{
	uint8_t		com_type;				// 命令类型 0x01,0x81;0x13,0x93
	uint8_t		gpio;					// bit[0]:gpio1。bit[1]:gpio2。...bit[7]:gpio8
	uint8_t		gpio_level;				// 对应每个gpio的高低电平
}gpio_t;


/*
读写器频率区域
China1  0x01 
China2  0x02 
Europe  0x03 
USA		0x04 
Korea	0x05 
Japan	0x06
*/
// 读写器频率区域
typedef	struct	_frequency_region
{
	uint8_t		com_type;				// 命令类型 0x09,0x89;0x11 ,0x91
	uint8_t		save_setting;			// 保存设置标志.说明：保存设置标志为0时，不保存设置，为1时保存设置。
	uint8_t		region;					// 读写器频率区域
}frequency_region_t;


// 锁定标签
#define	MASK_AND_OPERATION_LEN		3	
typedef	struct	_lock
{
	uint8_t		com_type;				// 命令类型 0x1B,0x9B;
	uint8_t		password[PASSWORD_LEN];	// 密码
	uint8_t		filter_mem_bank;		// 要过滤的标签的内存区.0为epc区，1为tid区
	uint8_t		filter_data_len_msb;	
	uint8_t		filter_data_len_lsb;
	tid_t		tid;
	epc_t		epc;
	uint8_t		mask_operation[MASK_AND_OPERATION_LEN];
#ifdef FOUR_CHANNELS
	uint8_t		ant_id;					// 天线号
#endif	
}lock_t;


// 销毁标签
typedef	struct	_kill
{
	uint8_t		com_type;				// 命令类型 0x1C,0x9C;
	uint8_t		password[PASSWORD_LEN];	// 密码
	uint8_t		filter_mem_bank;		// 要过滤的标签的内存区.0为epc区，1为tid区
	uint8_t		filter_data_len_msb;	
	uint8_t		filter_data_len_lsb;
	tid_t		tid;
	epc_t		epc;
#ifdef FOUR_CHANNELS
	uint8_t		ant_id;					// 天线号
#endif
}kill_t;

// 频点
#define	OUTPUT_FREQUENCY_NUM		128
typedef	struct	_output_frequency
{
	uint8_t		com_type;				// 命令类型 0x02 ,0x0D;
	int8_t		frequency_num;			// 频点个数
	float		frequency[OUTPUT_FREQUENCY_NUM];	// 输出频率
}output_frequency_t;



/*

说明：波特率设置成功后，需要重启模块，设置才能生效。

rate_type=0，对应设置值9600，
rate_type=1，对应设置值19200，
rate_type=2，对应设置值38400，
rate_type=3，对应设置值57600，
rate_type=4，对应设置值115200，
其他，非法值。
*/
#define		BAUD_RATE_9600			0
#define		BAUD_RATE_19200			1
#define		BAUD_RATE_38400			2
#define		BAUD_RATE_57600			3
#define		BAUD_RATE_115200		4
// 设置模块通讯波特率
typedef	struct _baud_rate
{
	uint8_t		com_type;				// 命令类型 0x23,0xA3;
	uint8_t		rate_type;				// 波特率 类型
}baud_rate_t;


#define		FASTID_ON				(1)
#define		FASTID_OFF				(0)
//说明：开启状态：fastid_switch为0x01；关闭状态：fastid_switch为0x00。
typedef struct	_fastid
{
	uint8_t		com_type;				// 命令类型 0x21,0xA1;0x22,0xA2;	
	uint8_t		fastid_switch;			// fastid 的开关。
}fastid_t;
//-------------------------------------------------------------------------
//说明：开启状态：tagFocus_switch为0x01；关闭状态：tagFocus_switch为0x00。
typedef struct	_tagFocus  
{
	uint8_t		com_type;				// 命令类型 0x29,0xA9;0x2A,0xAA;	
	uint8_t		tagFocus_switch;			// fastid 的开关。
}tagFocus_t;

//-------------------------------------------------------------------------

/*
说明：qtparam_closer为0，不启用近距离控制，为1启用近距离控制。
qtparam_status为0标签类型为Private Memory Map，为1表示标签使用Public Memory Map。
*/
typedef struct  _qtparam 
{
	uint8_t		com_type;			// 命令类型 0x21,0xA1;0x22,0xA2;
	uint8_t		password[PASSWORD_LEN];	// 密码
	uint8_t		filter_mem_bank;		// 要过滤的标签的内存区.0为epc区，1为tid区
	uint8_t		filter_data_len_msb;	
	uint8_t		filter_data_len_lsb;
	tid_t		tid;
	epc_t		epc;
	uint8_t		qtparam_closer;			// 启动近距离控制
	uint8_t		qtparam_status;			// 标签类型设置
}qtparam_t;	


/*
说明：qtparam_closer为0，不启用近距离控制，为1启用近距离控制。
qtparam_status为0标签类型为Private Memory Map，为1表示标签使用Public Memory Map。
*/
typedef struct  _qtpek 
{
	uint8_t		com_type;				// 命令类型 0x28,0xA8
	uint8_t		password[PASSWORD_LEN];	// 密码
	uint8_t		filter_mem_bank;		// 要过滤的标签的内存区.0为epc区，1为tid区
	uint8_t		filter_data_len_msb;	
	uint8_t		filter_data_len_lsb;
	tid_t		tid;
	epc_t		epc;
	uint8_t		oper_type;				// 读写操作
	uint8_t		qtpek_closer;			// 启动近距离控制
	uint8_t		mem_bank;				// 要读写的标签的内存区
	uint8_t		start_addr_msb;			// 要读写的标签的内存区的开始位置的高8位
	uint8_t		start_addr_lsb;			// 要读写的标签的内存区的开始位置的低8位
	uint8_t		data_len_msb;			// 要读写的数据长度的高8位，单位为字
	uint8_t		data_len_lsb;			// 要读写的数据长度的低8位，单位为字		
	uint8_t		data[PACKET_MAX];		// 要读写的数据
}qtpek_t;	

/*
说明：q_data 0 表示固定Q 算法，1 表示动态Q 算法
注意：在固定 Q 算法下，Q 固定为StartQ，忽略MinQ 和MaxQ。
startQ 设置：4bit 表示，0 至15
MinQ 设置：4bit 表示，0 至15
MaxQ 设置：4bit 表示，0 至15
*/
typedef struct  _genparam 
{
	uint8_t		com_type;			// 命令类型 0x21,0xA1;0x22,0xA2;
	uint8_t		q_data;				// 动态Q算法
	uint8_t		start_q;			// start_q 
	uint8_t		min_q;				// min_q
	uint8_t		max_q;				// max_q

	uint8_t     select_q;           //select_q
	uint8_t     session_q;          //session_q
	uint8_t     target_q;           //target_q
}genparam_t;	


#define		CARRIER_ON				(1)
#define		CARRIER_OFF				(0)
//说明：开启状态：carrier_switch为0x01；关闭状态：carrier_switch为0x00。
typedef struct	_carrier
{
	uint8_t		com_type;				// 命令类型 0x05,0x85;0x0F,0x8F;	
	uint8_t		carrier_switch;			// carrier 的开关。
}carrier_t;


/*
说明：RF link_profile_type
0x00 DSB_ASK / FM0 / 40KHz
0x01 PR _ASK / Miller4 / 250KHz
0x02 PR _ASK / Miller4 / 300KHz
0x03 DSB_ASK / FM0 / 400KHz
*/
typedef struct	_rf_link_profile
{
	uint8_t		com_type;				// 命令类型 0x03,0x83;0x0E,0x8E;	
	uint8_t		link_profile_type;		// rf_link_profile 的类型。
}rf_link_profile_t;


/*
说明：0x00—MAC 寄存器，0x01—OEM 寄存器，0x02—ByPass 寄存器
*/
typedef struct	_register
{
	uint8_t		com_type;				// 命令类型 0x03,0x83;0x0E,0x8E;	
	uint8_t		reg_type;				// 寄存器類型
	uint32_t	reg_addr;				// 寄存器地址
	uint32_t	reg_data;				// 寄存器數據。
}register_t;


#pragma pack(pop)    //恢复对齐状态 


#define  SANRAY_DLL

#ifdef SANRAY_DLL
#define SANRAY_API extern "C" __declspec(dllexport)
#else
#define SANRAY_API extern "C" __declspec(dllimport)
#endif

/**
  * @}
  */



/** @defgroup Doxygen_Demo_Public_Variable
  * @brief 声明公有全局变量
  * @{
  */

/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_Function
  * @brief 定义公有函数
  * @{
  */ 
// 传输操作


SANRAY_API	int32_t	transfer_interface_init(transfer_t trans);				// 初始化传输函数
SANRAY_API	int32_t transfer_init(void * param);							// 初始化 
SANRAY_API	int32_t transfer_open(void * param);							// 打开通讯
SANRAY_API	int32_t transfer_close(void * param);							// 关闭通讯
SANRAY_API	int32_t transfer_send(uint8_t *send_buffer,int32_t data_len);	// 发送
SANRAY_API	int32_t transfer_send_set(void * param);						// 发送设置
SANRAY_API	int32_t transfer_recv(uint8_t * recv_buffer);					// 接收
SANRAY_API	int32_t transfer_recv_set(void * param);						// 接收设置

// 命令操作
SANRAY_API	int32_t	command(const int32_t command_type,void* param);		// 外部接口函数

#define		CONNECT_BASIC		(100)
#define		CONNECT_SERIAL		(CONNECT_BASIC+1)
#define		CONNECT_NET			(CONNECT_BASIC+2)

SANRAY_API int basic_init(const int connect_type);					// 连接类型
/**
  * @}
  */



/**
  * @}
  */
/**
  * @}
  */
#ifdef __cplusplus
}
#endif

#endif
/* END OF FILE ---------------------------------------------------------------*/
