/**
  ******************************************************************************
  * @file      transfer.h
  * @author    sanray
  * @version   V3.0.0
  * @date      2013.08.26
  * @brief     类型定义
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TRANSFER_H__
#define __TRANSFER_H__

/* Include -------------------------------------------------------------------*/

#include <Windows.h>
#include <iostream>
#include <string>
using namespace std;


#include "types.h"


#ifdef __cplusplus
extern "C" {
#endif
/** @addtogroup Drivers
  * @{
  */
/** @addtogroup Doxygen_Demo
  * @{
  */



/**
  ******************************************************************************
  * @defgroup Doxygen_Demo_Configure
  * @brief 用户配置
  ******************************************************************************
  * @{
  */
//#define __EZOS_TASK_NUM 16 ///< 设置最大任务数
/**
  * @}
  */



/** @defgroup Doxygen_Demo_Public_TypeDefine
  * @brief 公有类型定义
  * @{
  */
	typedef struct _uart_open
	{
		//	HANDLE	com_handle;
		int8_t	com_name[128];
		int32_t	com_baudrate;
		int32_t	error_code;
	}uart_open_t;

	typedef	struct _uart_close
	{
		//	HANDLE	com_handle;
		int32_t	error_code;
	}uart_close_t;

	typedef	struct _uart_send
	{
		//	HANDLE	com_handle;
		int32_t	error_code;
	}uart_send_t;

	typedef	struct _uart_recv
	{
		//	HANDLE	com_handle;
		int32_t	error_code;
	}uart_recv_t;
/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_MacroDefine
  * @brief 公有宏定义
  * @{
  */
#define	UART_MAGIC				3000
	// 网络通讯异常
#define	ERROR_UART_CREAT	(UART_MAGIC + 1)	
#define	ERROR_UART_OPEN		(UART_MAGIC + 2)

#define	NET_MAGIC				2000
// 网络通讯异常
#define	NET_ERROR_SOCKET_INIT	(NET_MAGIC + 1)	
#define	NET_ERROR_SOCKET_ACCEPT	(NET_MAGIC + 2)
#define	NET_ERROR_SOCKET_CONNECT (NET_MAGIC + 3)

/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_Variable
  * @brief 声明公有全局变量
  * @{
  */

/**
  * @}
  */

/** @defgroup Doxygen_Demo_Public_Function
  * @brief 定义公有函数
  * @{
  */ 

/**
  * @}
  */



/**
  * @}
  */
/**
  * @}
  */
#ifdef __cplusplus
}
#endif

#endif
/* END OF FILE ---------------------------------------------------------------*/

