#pragma once

#include <QtWidgets/QMainWindow>
#include "os.h"
#include "ui_ledwin.h"
#include "monitor.h"

class LedWin: public QMainWindow
{
	Q_OBJECT

public:
	LedWin(QWidget *parent = 0);
	~LedWin();
    void showLogo(int size);

private:
	Ui::LedWinClass ui;
	monitor *m_monitor;
    QLabel *m_logo;

public slots:
	void exitClick();

protected:
    void customEvent(QEvent *event);
};
