#pragma once

#include <QtWidgets/QMainWindow>
#include <qlistwidget.h>

void initLog(QListWidget *w);

void errLog(QString str);
void okLog(QString str);
void warLog(QString str);
void infoLog(QString str);
void logD(QString str);

#ifdef Q_OS_WIN
void setup_console();
void release_console();
#else
inline void setup_console(){};
inline void release_console(){};
#endif

