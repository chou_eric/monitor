#ifndef CONFIG_H
#define CONFIG_H

#include <qstring.h>

const QString GRP_CAMERA = "camera";
  const QString KEY_IP = "IP";
  const QString KEY_USR = "user";
  const QString KEY_PASSWD = "passwd";

const QString GRP_RFREADER = "rfreader";
  const QString KEY_CAPTAL = "captal";
  const QString KEY_LOGOSIZE = "logosize";
  const QString KEY_DEBUG = "debug";
  const QString KEY_SHOOTANT = "shootAnt";
  const QString KEY_KILLANT = "killAnt";

const QString GRP_LED = "led";
  const QString KEY_STRFONT = "strFont";
  const QString KEY_STRCOLOR = "strColor";

  const QString KEY_PREFONT = "preFont";
  const QString KEY_PRECOLOR = "preColor";

  const QString KEY_COUNTFONT = "countFont";
  const QString KEY_COUNTCOLOR = "countColor";

  const QString KEY_MATCHFONT = "matchFont";
  const QString KEY_MATCHCOLOR = "matchColor";

  const QString KEY_BACKCOLOR = "backcolor";

class Config {
public:
    Config();
    ~Config();
    void writeIni(QString group, QString key, QString value);
    void readIni(QString group, QString key, QString &value);

private:
    QString m_filename;

};

#endif // CONFIG_H
