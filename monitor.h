#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_monitor.h"
#include "camera.h"
#include "RfReader.h"
#include "tagmatchwidget.h"
#include "config.h"
#include "event.h"

#include "mlabel.h"

#include "os.h"

#include <qtimer.h>
#include <qsound.h>

enum OpMode {
	mode_None,
	mode_Kill,     // 宰杀模式
	mode_Shoot,    // 录制模式
};

class monitor : public QMainWindow
{
	Q_OBJECT

public:
	monitor(QWidget *parent = 0);
	~monitor();

public:
	Camera *m_camera;
	RfReader *m_reader;
	tagMatchWidget *m_tagMatchWidget;
	bool m_threadRunning;
	OpMode m_mode;
	bool m_captal;
    int m_logoSize;
	tagFile *m_tagEvent;
	QWidget *m_led;
	int m_shootAnt;
	int m_killAnt;

public:
	void addTagSum();
	void clearTagSum();

	void setupLed(QWidget *w, QLabel *label, QLabel *pre, QLabel *count, QLabel *mode);
	void setLedString(QString str);
	void setLedFont(const QFont &font);
	void setLedForeColor(const QColor &color);
	void setLedBackColor(const QColor &color);
	void playSound();
	void showMatch(bool isMatch);

public slots:
	void startRealPlay();
	void stopRealPlay();
	void capturePic();
	void clearLog();
	void getRfPower();
	void setRfPower();
	void startKill();
	void stopKill();
	void setAnt();
	void getAnt();
	void setCameraDevCfg();
	void getCameraDevCfg();
	void setRfReaderCfg();
	void getRfReaderCfg();
	void setDebugConsole(int state);
	void chooseBackColor();
	void chooseStrFont(); void chooseStrColor();
	void choosePreFont(); void choosePreColor();
	void chooseCountFont(); void chooseCountColor();
	void chooseMatchFont(); void chooseMatchColor();
	void shootAntChanged(const QString &text);
	void killAntChanged(const QString &text);

private:
	Ui::monitorClass ui;
	Config *m_config;

	HANDLE m_threadHandle;
	DWORD m_threadId;

	QString m_workDir;
	QString m_currentTimeStr;

	MLabel *m_strLabel;
	MLabel *m_preLabel;
	MLabel *m_countLabel;
	MLabel *m_matchLabel;
	QColor m_backColor;

	QSound *m_sound;

	bool m_showDebug;
	QEvent::Type m_eventNoMatch;

private:
	bool initReader();
	bool initCamera();
	void getCameraConfig(QString &ip, QString &user, QString &passwd);
	void getRfReaderConfig();
	void initDir();
	void updateCurrentTimeStr();
	void fmtEventName(QString &out);
	void fmtVideoName(QString &out);
	void getDebugConsoleCfg();
	void setBackColor(const QColor &color);

protected:
	void customEvent(QEvent *event);
};
