#include "led.h"
#include "os.h"

#pragma execution_character_set ("utf-8")

LedWin::LedWin(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	ui.ledLabel->setAlignment(Qt::AlignHCenter);
	ui.ledLabel->setText("00000000");

	m_monitor = new monitor(this);
	m_monitor->show();
	m_monitor->setupLed(this, ui.ledLabel, ui.preLabel, ui.countLabel, ui.matchLabel);

    m_logo = new QLabel(this);
    showLogo(m_monitor->m_logoSize);
	showFullScreen();
}

LedWin::~LedWin()
{
	delete m_monitor;
    delete m_logo;
}

// exitBtn slot
void LedWin::exitClick()
{
	qApp->quit();
}

void LedWin::showLogo(int size)
{
    QImage* img = new QImage("./logo.png");
    QImage* resImg = new QImage;
    *resImg = img->scaled(size, size, Qt::KeepAspectRatio);
    m_logo->setPixmap(QPixmap::fromImage(*resImg));
    m_logo->move(50, 50);
    m_logo->resize(size, size);
    delete img;
    delete resImg;
}

void LedWin::customEvent(QEvent *event)
{
    if (event->type() == Event_changeLogoSize) {
        showLogo(m_monitor->m_logoSize);
    }
}
