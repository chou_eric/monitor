#ifndef CAMERA_H
#define CAMERA_H

#include "os.h"

#ifdef Q_OS_WIN

#include <stdio.h>
#include <iostream>
#include "Windows.h"
#include "HCNetSDK.h"

class Camera
{
public:
	Camera(QString ip, QString user, QString passwd);
	~Camera();

	bool initConnection();

	void getDeviceCfg();
	void getWorkStatus();

	bool setShowString(const char *str, WORD x, WORD y);
	bool disableShowString();

	bool startRealtimePlay(QString name);
	void stopRealtimePlay();
	bool capturePic();

private:
	void getShowString(NET_DVR_SHOWSTRING_V30 *p);
	void fmtCaptureName(QString &name);

private:
	LONG m_userID;
	LONG m_channel;
	LONG m_realPlayHandle;
	QString m_ipAddr;
	QString m_userName;
	QString m_passwd;
	bool m_initOk;

	bool m_realStarted;
};

#endif

#endif // CAMERA_H
