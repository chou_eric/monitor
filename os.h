#pragma execution_character_set ("utf-8")

#ifndef OS_H
#define OS_H

#include <qevent.h>

extern QEvent::Type Event_changeLogoSize;

#ifdef Q_OS_WIN

#include <Windows.h>

#else

#include <HCNetSDK.h>
#define WINAPI
#include <stdint.h>
typedef uint8_t byte;
#define sprintf_s sprintf

#define WaitForSingleObject(a, b)
#define CreateThread(a, b, c, d, e, f) 0
#define INFINITE 0

class Camera
{
public:
    Camera(QString ip, QString user, QString passwd){};
    ~Camera(){};

    bool initConnection(){return true; };

    void getDeviceCfg(){};
    void getWorkStatus(){};

    bool setShowString(const char *str, WORD x, WORD y){return true;};
    bool disableShowString(){return true;};

    bool startRealtimePlay(QString name){return true;};
    void stopRealtimePlay(){};
    bool capturePic(){return true;};
};

#include "RfReader.h"
class RfReader
{
public:
    RfReader(){};
    ~RfReader(){};
    bool InitTransfer(){return true;};
    void DeinitTransfer(){};
    void PrintNetDevice(device_t *dev){};

    bool GetFirmwareVersion(char *version, size_t len){return true;};

    /* Read 参数大小直接影响到天线读取标签距离，数值范围(5 到 30db)数值大读取距离远反之则小*/
    bool GetPower(uint8_t &read_power, uint8_t &write_power){return true;};
    bool SetPower(uint8_t read_power, uint8_t write_power){return true;};

    /* 连续获取Tag */
    bool StartMultiQuery_v2(){return true;};
    bool ReadMultiQuery_v2(tag_t *tag){return true;};
    bool StopMultiQuery_v2(){return true;};

    /* 设置天线使能与否 */
    bool GetCurrentAntanne(byte &number){return true;};
    bool SetCurrentAntanne(byte number){return true;};

    /* 设置天线工作时间和等待时间 */
    bool GetWorkTime(ushort &t1, ushort &t2, ushort &t3, ushort &t4, ushort &wt){return true;};
    bool SetWorkTime(ushort t1, ushort t2, ushort t3, ushort t4, ushort wt){return true;};
};

#endif

#endif // OS_H
