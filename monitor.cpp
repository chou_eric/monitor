﻿#include "monitor.h"
#include "log.h"
#include "handle.h"
#include "event.h"
#include "version.h"
#include <qdir.h>
#include <qcolordialog.h>

#include "os.h"

#pragma execution_character_set ("utf-8")

monitor::monitor(QWidget *parent)
	: QMainWindow(parent)
{
	bool initOk = true; 

	m_threadHandle = NULL;
	m_threadId = 0;
	m_threadRunning = false;
	m_mode = mode_None;
	m_tagEvent = NULL;

	m_sound = new QSound("./sound.wav");
	m_eventNoMatch = (QEvent::Type)QEvent::registerEventType();

	// init UI
	ui.setupUi(this);
	m_tagMatchWidget = new tagMatchWidget;
	ui.vLayout->addWidget(m_tagMatchWidget);
	for (int i = RF_MIN_POWER; i <= RF_MAX_POWER; i++) {
		char str[10];
		sprintf_s(str, "%d", i);
		ui.setPowerBox->addItem(str);
	}
	ui.rfReaderCaptalBox->addItem("true");
	ui.rfReaderCaptalBox->addItem("false");
	ui.versionLabel->setText(QString("Version: ") + QString(VERSION));
	ui.sumLineEdit->setReadOnly(true);
	ui.sumLineEdit->setText("0");

	for (int i = 1; i <= 4; i++) {
		QString s = QString("%1").arg(i);
		ui.shootAntBox->addItem(s);
		ui.killAntBox->addItem(s);
	}
	connect(ui.shootAntBox, SIGNAL(currentIndexChanged(QString)),
		this, SLOT(shootAntChanged(QString)));
	connect(ui.killAntBox, SIGNAL(currentIndexChanged(QString)),
		this, SLOT(killAntChanged(QString)));

	m_config = new Config();
	getCameraDevCfg();
	getRfReaderCfg();

	getDebugConsoleCfg();

	// init Log
	initLog(ui.logWidget);
	connect(ui.logWidget->model(), SIGNAL(rowsInserted(const QModelIndex &, int, int)),
			ui.logWidget, SLOT(scrollToBottom()));

	// init Directory
	initDir();

	// init Camera
	QString ip, user, passwd;
	getCameraConfig(ip, user, passwd);
	m_camera = new Camera(ip, user, passwd);
	if (initCamera() == false) {
		initOk = false;
		errLog("摄像头初始化失败!");
	}

	// init RfReader
	m_reader = new RfReader;
	if (initReader() == false) {
		initOk = false;
		errLog("阅读器初始化失败!");
	} else {
		getRfPower();
		getAnt();
	}
	getRfReaderConfig(); // read m_captal from ini file.

	if (initOk)
		okLog("初始化全部 OK.");
	else
		errLog("初始化有错误.");

	setWindowFlags(windowFlags() & ~(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint));
}

monitor::~monitor()
{
	if (m_threadRunning) {
		m_threadRunning = false;
		WaitForSingleObject(m_threadHandle, INFINITE);
	}
	delete m_camera;
	delete m_reader;
	delete m_tagMatchWidget;
	delete m_config;
	delete m_sound;
	//delete m_ledWin;
	if (m_tagEvent)
		delete m_tagEvent;

	if (m_showDebug)
		release_console();
}

void monitor::customEvent(QEvent *event)
{
	if (event->type() == QEvent::User) {
		m_matchLabel->setText("发现目标");
	} else if (event->type() == (m_eventNoMatch)) {
		m_matchLabel->setText("");
	}
}

// check directory for saving video file.
// if the directory dose not exist, create one.
void monitor::initDir()
{
	m_workDir = QDir::currentPath() + "/video";

	QDir dir(m_workDir);
	if (!dir.exists()) {
		dir.mkpath(m_workDir);
	}

	warLog(QString("视频保存目录为: %1").arg(m_workDir));
}

bool monitor::initCamera()
{
	if (m_camera->initConnection() == false) {
		return false;
	}
	m_camera->getDeviceCfg();
	m_camera->setShowString("ZeShanJu", 200, 300);
	ui.startRealPlayBtn->setDisabled(false);
	ui.stopRealPlayBtn->setDisabled(true);
	ui.capturePicBtn->setDisabled(true);
	return true;
}

bool monitor::initReader()
{
	if (m_reader->InitTransfer() == false) {
		return false;
	}

	m_reader->StopMultiQuery_v2();

	char version[64] = { 0 };
	m_reader->GetFirmwareVersion(version, sizeof(version));
	infoLog(QString("阅读器固件版本: %1").arg(version));

	byte ant = 0;
	if (m_reader->GetCurrentAntanne(ant)) {
		QString log("使能天线: ");
		for (int i = 0; i < RF_ANT_NUM; i++) {
			if (ant & (1 << i)) {
				char id[10];
				sprintf_s(id, "[%d] ", i + 1);
				log += id;
			}
		}
		infoLog(log);
	}

	okLog("阅读器初始化成功.");
	return true;
}

void monitor::getCameraConfig(QString &ip, QString &user, QString &passwd)
{
	m_config->readIni(GRP_CAMERA, KEY_IP, ip);
	m_config->readIni(GRP_CAMERA, KEY_USR, user);
	m_config->readIni(GRP_CAMERA, KEY_PASSWD, passwd);
}

void monitor::getRfReaderConfig()
{
	QString val;
	m_config->readIni(GRP_RFREADER, KEY_CAPTAL, val);
	if (val == "true")
		m_captal = true;
	else
		m_captal = false;

    m_config->readIni(GRP_RFREADER, KEY_LOGOSIZE, val);
    m_logoSize = val.toInt();

	infoLog(QString("是否显示大写: %1").arg(m_captal));
    infoLog(QString("Logo图像大小: %1").arg(m_logoSize));

	m_config->readIni(GRP_RFREADER, KEY_SHOOTANT, val);
	m_shootAnt = val.toInt();
	infoLog(QString("录制天线为: %1").arg(m_shootAnt));
	ui.shootAntBox->setCurrentIndex(m_shootAnt - 1);

	m_config->readIni(GRP_RFREADER, KEY_KILLANT, val);
	m_killAnt = val.toInt();
	infoLog(QString("宰杀天线为: %1").arg(m_killAnt));
	ui.killAntBox->setCurrentIndex(m_killAnt - 1);
}

void monitor::showMatch(bool isMatch)
{
	if (isMatch)
		QCoreApplication::postEvent(this, new QEvent(QEvent::User));
	else
		QCoreApplication::postEvent(this, new QEvent(m_eventNoMatch));
}

void monitor::addTagSum()
{
	uint32_t val = ui.sumLineEdit->text().toInt();
	val++;
	QString v = QString::number(val, 10);
	ui.sumLineEdit->setText(v);
	m_countLabel->setText("sum: "+v);
}

void monitor::clearTagSum()
{
	setLedString("00000000");
	m_countLabel->setText("sum: 0");
	ui.sumLineEdit->setText("0");
}

void monitor::updateCurrentTimeStr()
{
#ifdef Q_OS_WIN
	SYSTEMTIME t;
    GetLocalTime(&t);

	char s[64] = {0};
    sprintf(s, "%04d-%02d-%02d-%02d_%02d_%02d", t.wYear, t.wMonth,
			t.wDay, t.wHour, t.wMinute, t.wSecond);
	m_currentTimeStr = QString(s);
#else
    m_currentTimeStr = QString("Not implement in non-win.");
#endif
}

void monitor::fmtVideoName(QString &out)
{
	out = m_workDir + "/" + m_currentTimeStr + ".mp4";
}

void monitor::fmtEventName(QString &out)
{
	out = m_workDir + "/" + m_currentTimeStr + ".log";
}

void monitor::setupLed(QWidget *w, QLabel *label, QLabel *pre, QLabel *count, QLabel *mode)
{
	m_led = w;

	m_strLabel = new MLabel(label, m_config, "str");
	m_preLabel = new MLabel(pre, m_config, "pre");
	m_countLabel = new MLabel(count, m_config, "count");
	m_matchLabel = new MLabel(mode, m_config, "mode");
#ifdef Q_OS_WIN
    m_matchLabel->setText("");
#else
    m_matchLabel->setText("发现目标");
#endif

	QString colorStr;
    m_config->readIni(GRP_LED, KEY_BACKCOLOR, colorStr);
    m_backColor.setNamedColor(colorStr);
    setBackColor(m_backColor);

	m_countLabel->setText("sum: 0");
}

void monitor::setLedString(QString str)
{
	m_preLabel->setText("pre: "+m_strLabel->text()); 
	m_strLabel->setText(str);
}

void monitor::playSound()
{
	m_sound->play();
}

void monitor::setBackColor(const QColor &color)
{
    QPalette pa;
    pa.setColor(QPalette::Background, color);
    setAutoFillBackground(true);
    m_led->setPalette(pa);
	m_backColor = color;
}

////////////////////////////////////////////////////////////////////////////////

/* slot */
void monitor::startRealPlay()
{
	if (m_mode != mode_None) {
		errLog("当前模式不是空闲");
		return;
	}

	QString videoFilename, eventName;
	updateCurrentTimeStr();
	fmtVideoName(videoFilename);

	infoLog("--------------------------------------------");

	fmtEventName(eventName);
	m_tagEvent = new tagFile(eventName);
	if (m_tagEvent == NULL) {
		errLog("创建tag文件失败.");
		return;
	}

	if (m_camera->startRealtimePlay(videoFilename) == false) {
		errLog("启动录制失败: 摄像头启动实时播放失败.");
		return;
	} else {
		m_tagEvent->addStartEvent();
		okLog("启动摄像头拍摄成功.");
	}

	m_threadRunning = true;
	m_threadHandle = CreateThread(NULL, 0, readerThread, this, 0, &m_threadId);
	if (m_threadHandle == NULL) {
		errLog("启动录制失败: 创建阅读器线程失败.");
		m_camera->stopRealtimePlay();
		m_threadRunning = false;
		return;
	}

	// 录制按键
	ui.startRealPlayBtn->setDisabled(true);
	ui.stopRealPlayBtn->setDisabled(false);
	ui.capturePicBtn->setDisabled(false);
	// 宰杀按键
	ui.startKillBtn->setDisabled(true);
	ui.stopKillBtn->setDisabled(true);

	m_mode = mode_Shoot;

	okLog("录制开始...");
}

/* slot */
void monitor::stopRealPlay()
{
	infoLog("++++++++++++++++++++++++++++++++++++++++++++");
	m_threadRunning = false;
	WaitForSingleObject(m_threadHandle, INFINITE);
	m_camera->stopRealtimePlay();

	// 录制按键
	ui.startRealPlayBtn->setDisabled(false);
	ui.stopRealPlayBtn->setDisabled(true);
	ui.capturePicBtn->setDisabled(true);
	// 宰杀按键
	ui.startKillBtn->setDisabled(false);
	ui.stopKillBtn->setDisabled(false);

	clearTagList();
	clearTagSum();
	m_mode = mode_None;
	m_tagEvent->addStopEvent();
	delete m_tagEvent;
	m_tagEvent = NULL;
	okLog("停止录制.");
}

/* slot */
void monitor::startKill()
{
	if (m_mode != mode_None) {
		errLog("当前模式不是空闲");
		return;
	}
	infoLog("--------------------------------------------");

	m_threadRunning = true;
	m_threadHandle = CreateThread(NULL, 0, readerThread, this, 0, &m_threadId);
	if (m_threadHandle == NULL) {
		errLog("启动宰杀失败: 创建阅读器线程失败.");
		m_threadRunning = false;
		return;
	}

	// 录制按键
	ui.startRealPlayBtn->setDisabled(true);
	ui.stopRealPlayBtn->setDisabled(true);
	ui.capturePicBtn->setDisabled(true);
	// 宰杀案件
	ui.startKillBtn->setDisabled(true);
	ui.stopKillBtn->setDisabled(false);

	m_mode = mode_Kill;
	okLog("开始宰杀...");
}

/* slot */
void monitor::stopKill()
{
	infoLog("++++++++++++++++++++++++++++++++++++++++++++");
	m_threadRunning = false;
	WaitForSingleObject(m_threadHandle, INFINITE);

	// 录制按键
	ui.startRealPlayBtn->setDisabled(false);
	ui.stopRealPlayBtn->setDisabled(true);
	ui.capturePicBtn->setDisabled(true);
	// 宰杀按键
	ui.startKillBtn->setDisabled(false);
	ui.stopKillBtn->setDisabled(true);

	clearTagList();
	clearTagSum();
	m_mode = mode_None;
	okLog("停止宰杀.");
}

/* slot */
void monitor::capturePic()
{
	m_camera->capturePic();
}

/* slot */
void monitor::clearLog()
{
	ui.logWidget->clear();
}

/* slot */
void monitor::getRfPower()
{
	uint8_t read_power, write_power;

	if (m_reader->GetPower(read_power, write_power) == false) {
		errLog("获取阅读器功率失败.");
		return;
	}
	char str[10];
	sprintf_s(str, "%d", read_power);
	ui.getPowerLineEdit->setText(str);
	okLog(QString("获取阅读器功率成功 [%1dBm]").arg(str));
}

/* slot */
void monitor::setRfPower()
{
	uint8_t read_power, write_power;
	if (m_reader->GetPower(read_power, write_power) == false) {
		errLog("获取阅读器功率失败.");
		return;
	}

	bool ok;
	read_power = ui.setPowerBox->currentText().toInt(&ok, 10);
	if (ok == false) {
		errLog("设置阅读器功率失败: 转换错误.");
		return;
	}

	if (m_reader->SetPower(read_power, write_power) == false) {
		errLog("设置阅读器功率失败.");
		return;
	}
	okLog(QString("设置阅读器功率为[%1dBm]成功").arg(read_power));
}

/* slot */
void monitor::setAnt()
{
	byte ant = 0;

	if (ui.antCheckBox_1->checkState() == Qt::Checked)
		ant |= (1 << 0);
	if (ui.antCheckBox_2->checkState() == Qt::Checked)
		ant |= (1 << 1);
	if (ui.antCheckBox_3->checkState() == Qt::Checked)
		ant |= (1 << 2);
	if (ui.antCheckBox_4->checkState() == Qt::Checked)
		ant |= (1 << 3);
	
	if (m_reader->SetCurrentAntanne(ant) == false) {
		errLog("设置天线配置失败");
	} else {
		okLog("设置天线配置成功");
	}

	ushort t1 = ui.workTimeLineEdit_1->text().toInt();
	if (t1 < 30 || t1 > 60000) {
		errLog("天线工作时间1超出范围");
		return;
	}

	ushort t2 = ui.workTimeLineEdit_2->text().toInt();
	if (t2 < 30 || t2 > 60000) {
		errLog("天线工作时间2超出范围");
		return;
	}

	ushort t3 = ui.workTimeLineEdit_3->text().toInt();
	if (t3 < 30 || t3 > 60000) {
		errLog("天线工作时间3超出范围");
		return;
	}

	ushort t4 = ui.workTimeLineEdit_4->text().toInt();
	if (t4 < 30 || t4 > 60000) {
		errLog("天线工作时间4超出范围");
		return;
	}

	ushort wt = ui.waitTimeLineEdit->text().toInt();
	if (wt < 0 || wt > 60000) {
		errLog("天线等待时间超出范围");
		return;
	}

	if (m_reader->SetWorkTime(t1, t2, t3, t4, wt) == false) {
		errLog("设置天线工作时间错误.");
		return;
	} else {
		okLog("设置天线工作时间成功.");
	}
}

/* slot */
void monitor::getAnt()
{
	byte ant = 0;
	if (m_reader->GetCurrentAntanne(ant) == false) {
		errLog("获取天线配置失败");
		return;
	}
	okLog("获取天线配置成功");

	if (ant & (1 << 0)) {
		ui.antCheckBox_1->setCheckState(Qt::Checked);
		ui.workTimeLineEdit_1->setEnabled(true);
	} else {
		ui.antCheckBox_1->setCheckState(Qt::Unchecked);
		ui.workTimeLineEdit_1->setEnabled(false);
	}

	if (ant & (1 << 1)) {
		ui.antCheckBox_2->setCheckState(Qt::Checked);
		ui.workTimeLineEdit_2->setEnabled(true);
	} else {
		ui.antCheckBox_2->setCheckState(Qt::Unchecked);
		ui.workTimeLineEdit_2->setEnabled(false);
	}

	if (ant & (1 << 2)) {
		ui.antCheckBox_3->setCheckState(Qt::Checked);
		ui.workTimeLineEdit_3->setEnabled(true);
	} else {
		ui.antCheckBox_3->setCheckState(Qt::Unchecked);
		ui.workTimeLineEdit_3->setEnabled(false);
	}

	if (ant & (1 << 3)) {
		ui.antCheckBox_4->setCheckState(Qt::Checked);
		ui.workTimeLineEdit_4->setEnabled(true);
	} else {
		ui.antCheckBox_4->setCheckState(Qt::Unchecked);
		ui.workTimeLineEdit_4->setEnabled(false);
	}

	ushort t1, t2, t3, t4, wt;
	if (m_reader->GetWorkTime(t1, t2, t3, t4, wt) == false) {
		errLog("获取天线工作时间失败");
		return;
	}

	ui.workTimeLineEdit_1->setText(QString::number(t1));
	ui.workTimeLineEdit_2->setText(QString::number(t2));
	ui.workTimeLineEdit_3->setText(QString::number(t3));
	ui.workTimeLineEdit_4->setText(QString::number(t4));
	ui.waitTimeLineEdit->setText(QString::number(wt));
}

/* slot */
void monitor::setCameraDevCfg()
{
    m_config->writeIni(GRP_CAMERA, KEY_IP, ui.cameraIPCfgLineEdit->text());
    m_config->writeIni(GRP_CAMERA, KEY_USR, ui.cameraUserCfgLineEdit->text());
    m_config->writeIni(GRP_CAMERA, KEY_PASSWD, ui.cameraPasswdCfgLineEdit->text());
}

/* slot */
void monitor::getCameraDevCfg()
{
	QString val;

	m_config->readIni(GRP_CAMERA, KEY_IP, val);
	ui.cameraIPCfgLineEdit->setText(val);

	m_config->readIni(GRP_CAMERA, KEY_USR, val);
	ui.cameraUserCfgLineEdit->setText(val);

	m_config->readIni(GRP_CAMERA, KEY_PASSWD, val);
	ui.cameraPasswdCfgLineEdit->setText(val);
}

/* slot */
void monitor::setRfReaderCfg()
{
	if (ui.rfReaderCaptalBox->currentIndex() == 0)
		m_config->writeIni(GRP_RFREADER, KEY_CAPTAL, "true");
	else
		m_config->writeIni(GRP_RFREADER, KEY_CAPTAL, "false");

    QString val = ui.logoSizeLineEdit->text();
    m_config->writeIni(GRP_RFREADER, KEY_LOGOSIZE, val);
    m_logoSize = val.toInt();
    QCoreApplication::postEvent(m_led, new QEvent(Event_changeLogoSize));
}

/* slot */
void monitor::getRfReaderCfg()
{
	QString val;

	m_config->readIni(GRP_RFREADER, KEY_CAPTAL, val);
	if (val == "true")
		ui.rfReaderCaptalBox->setCurrentIndex(0);
	else if (val == "false")
		ui.rfReaderCaptalBox->setCurrentIndex(1);
    m_config->readIni(GRP_RFREADER, KEY_LOGOSIZE, val);
    ui.logoSizeLineEdit->setText(val);
}

/* slot */
void monitor::setDebugConsole(int state)
{
	if (state == Qt::Unchecked)
		m_config->writeIni(GRP_RFREADER, KEY_DEBUG, "false");
	else
		m_config->writeIni(GRP_RFREADER, KEY_DEBUG, "true");
}

/* slot */
void monitor::getDebugConsoleCfg()
{
	QString val = "false";

	m_config->readIni(GRP_RFREADER, KEY_DEBUG, val);
	if (val == "true") {
		m_showDebug = true;
		ui.debugConsoleCheckBox->setCheckState(Qt::Checked);
		setup_console();
	} else if (val == "false") {
		ui.debugConsoleCheckBox->setCheckState(Qt::Unchecked);
		m_showDebug = false;
	}
}

void monitor::chooseBackColor()
{
    QColor color = QColorDialog::getColor(m_backColor);

    if (color.isValid()) {
        setBackColor(color);
        m_config->writeIni(GRP_LED, KEY_BACKCOLOR, color.name());
    }
}

void monitor::chooseStrFont() { m_strLabel->chooseFont(); }
void monitor::chooseStrColor() { m_strLabel->chooseColor(); }
void monitor::choosePreFont() { m_preLabel->chooseFont(); }
void monitor::choosePreColor() { m_preLabel->chooseColor(); }
void monitor::chooseCountFont() { m_countLabel->chooseFont(); }
void monitor::chooseCountColor() { m_countLabel->chooseColor(); }
void monitor::chooseMatchFont() { m_matchLabel->chooseFont(); }
void monitor::chooseMatchColor() { m_matchLabel->chooseColor(); }

void monitor::shootAntChanged(const QString &text)
{
	infoLog(QString("录制天线修改为: %1").arg(text));
	m_shootAnt = text.toInt();
	m_config->writeIni(GRP_RFREADER, KEY_SHOOTANT, text);
}

void monitor::killAntChanged(const QString &text)
{
	infoLog(QString("宰杀天线修改为: %1").arg(text));
	m_killAnt = text.toInt();
	m_config->writeIni(GRP_RFREADER, KEY_KILLANT, text);
}
