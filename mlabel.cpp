#include "mlabel.h"
#include <qfontdialog.h>
#include <qcolordialog.h>

MLabel::MLabel(QLabel *label, Config *cfg, QString key)
{
	m_label = label;
	m_config = cfg;
	m_fontKey = key + "Font";
	m_colorKey = key + "Color";

    QString fontStr;
    m_config->readIni(GRP_LED, m_fontKey, fontStr);
    m_font.fromString(fontStr);
	setFont(m_font);

    QString colorStr;
    m_config->readIni(GRP_LED, m_colorKey, colorStr);
    m_color.setNamedColor(colorStr);
    setColor(m_color);
}

MLabel::~MLabel()
{
}

void MLabel::setFont(QFont &f)
{
	m_label->setFont(f);
	m_font = f;
}

void MLabel::setColor(QColor &c)
{
	QPalette pa;
	pa.setColor(QPalette::WindowText, c);
	m_label->setPalette(pa);
	m_color = c;
}

void MLabel::chooseFont()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, m_font);
    if (ok) {
		setFont(font);
		m_config->writeIni(GRP_LED, m_fontKey, font.toString());
    }
}

void MLabel::chooseColor()
{
    QColor color = QColorDialog::getColor(m_color);

    if (color.isValid()) {
		setColor(color);
        m_config->writeIni(GRP_LED, m_colorKey, color.name());
    }
}

void MLabel::setText(const QString &text)
{
	m_label->setText(text);
}

QString MLabel::text() const 
{
	return m_label->text();
}
