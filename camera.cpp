#pragma execution_character_set ("utf-8")

#include "os.h"
#ifdef Q_OS_WIN

#include <string.h>
#include <qdir.h>

#include "camera.h"
#include "log.h"
#include <windows.h>

#define pinfo printf

////////////////////////////////////////////////////////////////////////////////

Camera::Camera(QString ip, QString user, QString passwd)
{
	m_ipAddr = ip;
	m_userName = user;
	m_passwd = passwd;
	m_realStarted = false;
}

Camera::~Camera()
{
	if (m_realStarted) {
		stopRealtimePlay();
		printf("%s(): stop real play\n", __FUNCTION__);
	}

	NET_DVR_Logout(m_userID);
	NET_DVR_Cleanup();
	return;
}

////////////////////////////////////////////////////////////////////////////////

bool Camera::initConnection()
{
	int i = 0;
	BYTE byIPID, byIPIDHigh;
	int iDevInfoIndex, iGroupNO, iIPCh;
	DWORD dwReturned = 0;

	QString log = QString("摄像头所连接的IP为: %1").arg(m_ipAddr);
	infoLog(log);

	NET_DVR_Init();
	NET_DVR_SetConnectTime(2000, 1);
	NET_DVR_SetReconnect(10000, true);

	NET_DVR_DEVICEINFO_V30 devInfo;
	m_userID = NET_DVR_Login_V30(m_ipAddr.toLatin1().data(), 8000,
			m_userName.toLatin1().data(), m_passwd.toLatin1().data(), &devInfo);
	if (m_userID < 0) {
		errLog("摄像头登录失败！");
		NET_DVR_Cleanup();
		m_initOk = false;
		return false;
	}

	m_channel = 1;
	m_initOk = true;

	okLog("摄像头初始化成功!");
	return true;
}

// get device configuration and information
void Camera::getDeviceCfg()
{
	if (!m_initOk) {
		warLog(QString("摄像头初始化有错误，无法执行该操作 %1.").arg(__FUNCTION__));
		return;
	}
	DWORD cmd = NET_DVR_GET_DEVICECFG_V40;
	NET_DVR_DEVICECFG_V40 devCfg;
	NET_DVR_DEVICECFG_V40 *p = &devCfg;
	DWORD ret;

	memset(&devCfg, 0, sizeof(devCfg));
	if (!NET_DVR_GetDVRConfig(m_userID, cmd, 0,
				&devCfg, sizeof(devCfg), &ret)) {
		pinfo("get dev cfg failed\n");
		return;
	}

#if 0
	printf("----------- dev cfg --------------\n");
	printf("  name %s, DVRID: %d\n", p->sDVRName, p->dwDVRID);
	printf("  cycle record: %d\n", p->dwRecycleRecord);
	printf("  serial: %s\n", p->sSerialNumber);
	printf("  software version: %d, build date: %d\n", p->dwSoftwareVersion, p->dwSoftwareBuildDate);
	printf("  alarm input port num: %d, out num: %d\n", p->byAlarmInPortNum, p->byAlarmOutPortNum);
	printf("  RS232 num: %d, RS485 num: %d\n", p->byRS232Num, p->byRS485Num);
	printf("  Network Port num: %d\n", p->byNetworkPortNum);
	printf("  DiskCtrlNum: %d, disk num: %d\n", p->byDiskCtrlNum, p->byDiskNum);
	printf("  DVR Type: %d, Dev Type: %d, Dev Type name: %s\n", p->byDVRType, 
			p->wDevType, p->byDevTypeName);
	printf("  channel num: %d, start channel: %d, Decoder channel: %d\n",
			p->byChanNum, p->byStartChan, p->byDecordChans);
	printf("  VGA num: %d, USB num: %d, Aux num: %d, Audio num: %d\n",
			p->byVGANum, p->byUSBNum, p->byAuxoutNum, p->byAudioNum);
	printf("  IP channel num: %d/%d, zero Channel num: %d\n",
			p->byHighIPChanNum, p->byIPChanNum, p->byZeroChanNum);
	printf("  Support: 0x%x, Support1: 0x%x, Support2: 0x%x\n", p->bySupport, 
			p->bySupport1, p->bySupport2);
	printf("  ESATA usage: %d, IPC plug: %d, Storage Mode: %d\n", p->byEsataUseage,
			p->byIPCPlug, p->byStorageMode);
#endif

	QString log = QString("摄像头型号: %1").arg((char *)p->byDevTypeName);
	infoLog(log);
}

// get channel work status
void Camera::getWorkStatus()
{
	if (!m_initOk) {
		warLog(QString("摄像头初始化有错误，无法执行该操作 %1.").arg(__FUNCTION__));
		return;
	}
	DWORD cmd = NET_DVR_GET_WORK_STATUS;
	NET_DVR_WORKSTATE_V40 state;
	NET_DVR_GETWORKSTATE_COND cond = {0};
	DWORD dwList = 0;
	int i, j;

	memset(&state,0, sizeof(NET_DVR_WORKSTATE_V40));
	cond.dwSize =  sizeof(NET_DVR_GETWORKSTATE_COND);
	cond.byFindChanByCond = 0;
	cond.byFindHardByCond = 0;

	if (!NET_DVR_GetDeviceConfig(m_userID, cmd, 0, &cond, sizeof(cond), &dwList,
				&state, sizeof(state))) {
		pinfo("get work status failed: %d\n", NET_DVR_GetLastError());
		return;
	}
	printf("dwlist = %d\n", dwList);
	if (dwList != 0 && dwList != 1) {
		pinfo("dwList failed: %d\n", dwList);
		return;
	}

	printf("  status: %d\n", state.dwDeviceStatic);
	for (i = 0; i < MAX_DISKNUM_V30; i++) {
		NET_DVR_DISKSTATE *s = &state.struHardDiskStatic[i];
		if (s->dwVolume == 0)
			continue;
		printf("  harddisk[%d]: %d, free %dMB; total %dMB\n", i, s->dwHardDiskStatic,
				s->dwFreeSpace, s->dwVolume);
	}
	for (i = 0; i < MAX_CHANNUM_V40; i++) {
		NET_DVR_CHANNELSTATE_V30 *s = &state.struChanStatic[i];
		if (s->dwChannelNo == 0xffffffff)
			break;
		printf("  channel number: %d\n", s->dwChannelNo);
		printf("    recording: %d, signal state: %d\n", s->byRecordStatic,
				s->bySignalStatic);
		printf("    hardware status: %d, bitrate: %d\n", s->byHardwareStatic,
				s->dwBitRate);
		for (j = 0; j < s->dwLinkNum; j++)
			printf("    link[%d]: %s\n", j, s->struClientIP[j].sIpV4);
		printf("    IP Link Num: %d\n", s->dwIPLinkNum);
	}
}

void Camera::getShowString(NET_DVR_SHOWSTRING_V30 *p)
{
	if (!m_initOk) {
		warLog(QString("摄像头初始化有错误，无法执行该操作 %1.").arg(__FUNCTION__));
		return;
	}
	DWORD cmd = NET_DVR_GET_SHOWSTRING_V30;
	DWORD ret;
	int i;
	
	memset(p, 0, sizeof(*p));
	if (!NET_DVR_GetDVRConfig(m_userID, cmd, m_channel, p, sizeof(*p), &ret)) {
		pinfo("get show string cfg failed: %d\n", NET_DVR_GetLastError());
		return;
	}

#if 0
	printf("------ show string cfg -----------------\n");
	for (i = 0; i < MAX_STRINGNUM_V30; i++)
	printf("  [%d]: show: %d, %s\n", i, p->struStringInfo[i].wShowString, 
			p->struStringInfo[i].sString);
#endif
}

// OSD. just use the first one.
bool Camera::setShowString(const char *str, WORD x, WORD y)
{
	if (!m_initOk) {
		warLog(QString("摄像头初始化有错误，无法执行该操作 %1.").arg(__FUNCTION__));
		return false;
	}
	DWORD cmd = NET_DVR_SET_SHOWSTRING_V30;
	NET_DVR_SHOWSTRING_V30 cfg;

	getShowString(&cfg);
	cfg.struStringInfo[0].wShowString = 1;
	cfg.struStringInfo[0].wStringSize = strlen(str);
	cfg.struStringInfo[0].wShowStringTopLeftX = x;
	cfg.struStringInfo[0].wShowStringTopLeftY = y;
	strcpy(cfg.struStringInfo[0].sString, str);

	if (!NET_DVR_SetDVRConfig(m_userID, cmd, m_channel, &cfg, sizeof(cfg))) {
		errLog("摄像头设置显示文字失败!");
		return false;
	}

	return true;
}

bool Camera::disableShowString()
{
	if (!m_initOk) {
		warLog(QString("摄像头初始化有错误，无法执行该操作 %1.").arg(__FUNCTION__));
		return false;
	}
	DWORD cmd = NET_DVR_SET_SHOWSTRING_V30;
	NET_DVR_SHOWSTRING_V30 cfg;

	getShowString(&cfg);
	cfg.struStringInfo[0].wShowString = 0;

	if (!NET_DVR_SetDVRConfig(m_userID, cmd, m_channel, &cfg, sizeof(cfg))) {
		errLog("摄像头禁用显示文字失败");
		return false;
	}
	return true;
}

static void CALLBACK g_ExceptionCallBack(DWORD dwType, LONG lUserID, LONG lHandle, void *pUser)
{
    char tempbuf[256] = {0};
	QString log = QString("摄像头录制错误: %1!").arg(dwType);
	errLog(log);
	switch(dwType) {
		case EXCEPTION_RECONNECT:
			warLog("摄像头需要重连.");
			break;
		default:
			break;
	}
}

// start realplay (but do not decode and preview) and save RealData to PC
bool Camera::startRealtimePlay(QString name)
{
	if (!m_initOk) {
		warLog(QString("摄像头初始化有错误，无法执行该操作 %1.").arg(__FUNCTION__));
		return false;
	}
	NET_DVR_PREVIEWINFO struPlayInfo = {0};
	struPlayInfo.hPlayWnd = 0;      // just fetch the stream, do not decode
	struPlayInfo.lChannel = m_channel;
	struPlayInfo.dwStreamType = 0; // major stream
	struPlayInfo.dwLinkMode = 0;   //TCP

	NET_DVR_SetExceptionCallBack_V30(0, NULL, g_ExceptionCallBack, NULL);

	m_realPlayHandle = NET_DVR_RealPlay_V40(m_userID, &struPlayInfo, NULL, NULL);
	if (m_realPlayHandle< 0) {
		QString log = QString("摄像头录制错误: [%1]").arg(NET_DVR_GetLastError());
		errLog(log);
		return false;
	}

	if (!NET_DVR_SaveRealData(m_realPlayHandle, name.toLatin1().data())) {
		QString log = QString("摄像头保存视频错误: [%1]").arg(NET_DVR_GetLastError());
		errLog(log);
		return false;
	}

	QString log = QString("开始录制，保存为: %1").arg(QFileInfo(name).fileName());
	okLog(log);
	m_realStarted = true;

	return true;
}

// stop realplay
void Camera::stopRealtimePlay()
{
	if (!m_initOk) {
		warLog(QString("摄像头初始化有错误，无法执行该操作 %1.").arg(__FUNCTION__));
		return;
	}
	infoLog("摄像头停止录制!");
	NET_DVR_StopSaveRealData(m_realPlayHandle);
	NET_DVR_StopRealPlay(m_realPlayHandle);
	m_realStarted = false;
}

// capture picture during realplay
bool Camera::capturePic()
{
	if (!m_initOk) {
		warLog(QString("摄像头初始化有错误，无法执行该操作 %1.").arg(__FUNCTION__));
		return false;
	}
	QString name;

	fmtCaptureName(name);

	NET_DVR_JPEGPARA para;
	para.wPicSize = 9;
	para.wPicQuality = 0;
	if (!NET_DVR_CaptureJPEGPicture(m_userID, m_channel, &para, name.toLatin1().data())) {
		QString log = QString("摄像头截图失败: [%1]").arg(NET_DVR_GetLastError());
		errLog(log);
		return false;
	}

	okLog("截图成功");
	return true;
}
	
void Camera::fmtCaptureName(QString &name)
{
	SYSTEMTIME t;
	GetLocalTime(&t);

	char s[64] = {0};
	sprintf(s, "%04d-%02d-%02d-%02d_%02d_%02d.jpg", t.wYear, t.wMonth + 1,
			t.wDay, t.wHour, t.wMinute, t.wSecond);
	name = QString(s);
}

#endif
