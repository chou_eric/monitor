#-------------------------------------------------
#
# Project created by QtCreator 2017-09-29T09:57:29
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = monitor
TEMPLATE = app

INCLUDEPATH += vendor/camera_32/include
INCLUDEPATH += vendor/rfid/include

SOURCES += main.cpp\
    tagmatchwidget.cpp \
    mlabel.cpp \
    log.cpp \
    led.cpp \
    handle.cpp \
    event.cpp \
    config.cpp \
    camera.cpp \
    RfReader.cpp \
    monitor.cpp

HEADERS  += version.h \
    tagmatchwidget.h \
    mlabel.h \
    log.h \
    led.h \
    handle.h \
    event.h \
    config.h \
    camera.h \
    RfReader.h \
    monitor.h \
    os.h

FORMS += \
    tagmatchwidget.ui \
    ledwin.ui \
    monitor.ui

OTHER_FILES += \
    README.md
