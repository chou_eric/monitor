﻿#include "event.h"
#include "log.h"

#include <QTime>
#include "os.h"

#pragma execution_character_set ("utf-8")

tagFile::tagFile(QString filename)
{
	m_file.setFileName(filename);
	if (!m_file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		errLog(QString("Event文件: %1 创建失败").arg(filename));
	} else {
		warLog(QString("Event文件为: [%1]").arg(filename));
	}
}

tagFile::~tagFile()
{
	m_file.close();
}

// do timestamp
void tagFile::formatEventHeader(char *str)
{
#ifdef Q_OS_WIN
	SYSTEMTIME t;
    GetLocalTime(&t);

    sprintf(str, "[%04d-%02d-%02d-%02d:%02d:%02d]", t.wYear, t.wMonth,
			t.wDay, t.wHour, t.wMinute, t.wSecond);
#else
    sprintf(str, "Not Implement in non-win paltfrom");
#endif
}

void tagFile::addEvent(QString event)
{
	QTextStream out(&m_file);    
	out << event << endl;    
	out.flush();    
}

// add 'start' event timestamp in log file
// add timestamp now.
void tagFile::addStartEvent()
{
	char header[32] = {0};
	formatEventHeader(header);

	addEvent(QString("%1 %2").arg(header, ">>> start >>>"));
}

// add 'tag' event timestamp in log file
void tagFile::addTagEvent(char *tag)
{ 
	char header[32] = {0};
	formatEventHeader(header);

	addEvent(QString("%1 %2").arg(header, tag));
}

void tagFile::addStopEvent()
{
	char header[32] = {0};
	formatEventHeader(header);

	addEvent(QString("%1 %2").arg(header, "||| stop |||"));
}
