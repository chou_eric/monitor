#pragma once

// SDK include files
#include <arch.h>
#include <error_code.h>
#ifdef Q_OS_WIN
#include <transfer.h>
#include <types.h>
#endif

#define DEVICE_AMOUNT 10

struct DEVICEINFO {
	char strMAC[20]; //转换器 MAC 地址,格式为 00.09.F6.01.02.03
	char strIP[20]; //转换器 IP 地址，格式为 10.1.1.1
	unsigned int dwModel; //转换器型号
	unsigned int dwVersion; //版本号
	char strName[20]; //名字
	char pbUnused[100];//未使用
};

struct C2000NETSETTING {
	int nNetIndex; //索引号，从 0 开始编号
	int bUseStaticIP; //0：自动 IP；1：指定 IP
	char ipAddr[20]; //C3000 的 IP 地址
	char ipNetMask[20]; //网络所在掩码
	char ipGateway[20]; //网关的 IP 地址
	char ipDnsServer[20]; //DNS 服务器的 IP 地址
	char pbUnused[200];//未使用
};

struct C2000SOCKETSETTING {
	int nComIndex; //串口号，该套接口参数是属于哪个串口的， 如果不属于任何串口，该值为负数，具体值另行解释
	int nSocketIndex;//该套接口在某个串口中编号，从 0 开始编号
	int nWorkMode; //0：TCP 客户；1：TCP 服务器；2：UDP1 或自动；3：UDP2；4：自动，根据具体的产品而定
	int nLocalPort; //本地端口
	char szPeerName[64]; //对方主机的域名或 IP
	int nPeerPort; //对方端口
	int bUseSocket; //是否使用 Socket。0：否，1：是
	byte pbUnused[200];//未使用
};

struct C2000COMSETTING {
	int nComIndex; //串口号，该串口设置参数是属于哪个串口的，从 0 开始编号
	int nBaudrate; //波特率
	int nDatabit; //数据位数
	int nParity; //校验方式
	int nStopbit; //停止位数
	int nFlowCtrl; //流控方式
	unsigned int nIntervalTimeout; //间隔超时时间（最少发送时间）
	unsigned int nMaxFrameLength; //最大帧长度（最小发送字节）
	int nInceptBytesLength; //包头长度
	byte pbInceptBytes[4]; //包头字符
	int nTermBytesLength; //包尾长度
	byte pbTermBytes[4]; //包尾字符
	int nWorkMode; //0：不变 1：232 2：485 3：422
	byte pbUnused[200];//未使用
};

typedef struct _device {
	DEVICEINFO device_info; // 设备信息数组
	C2000NETSETTING net_setting; // 模块的网络信息
	C2000SOCKETSETTING socket_setting; // 模块的socket信息
	C2000COMSETTING com_setting; // 模块的串口信息
}device_t;

typedef struct _net_device {
	int device_num; // 转换器的数量
	device_t net_device[DEVICE_AMOUNT];
} net_device_t;

////////////////////////////////////////////////////////////////////////////////

#define EPC_LEN 12

#define RF_MIN_POWER 5
#define RF_MAX_POWER 30

#define RF_ANT_NUM 4

typedef struct _tag_info {
	uint8_t epc[EPC_LEN];
	double rssi;   // uint: db
	int antanne_id; // KILL_ANT or SHOOT_ANT
} tag_t;

#ifdef Q_OS_WIN
class RfReader
{
public:
	RfReader();
	~RfReader();
	bool InitTransfer();
	void DeinitTransfer();
	void PrintNetDevice(device_t *dev);

	bool GetFirmwareVersion(char *version, size_t len);

	/* Read 参数大小直接影响到天线读取标签距离，数值范围(5 到 30db)数值大读取距离远反之则小*/
	bool GetPower(uint8_t &read_power, uint8_t &write_power);
	bool SetPower(uint8_t read_power, uint8_t write_power);

	/* 连续获取Tag */
	bool StartMultiQuery_v2();
	bool ReadMultiQuery_v2(tag_t *tag);
	bool StopMultiQuery_v2();

	/* 设置天线使能与否 */
	bool GetCurrentAntanne(byte &number);
	bool SetCurrentAntanne(byte number);

	/* 设置天线工作时间和等待时间 */
	bool GetWorkTime(ushort &t1, ushort &t2, ushort &t3, ushort &t4, ushort &wt);
	bool SetWorkTime(ushort t1, ushort t2, ushort t3, ushort t4, ushort wt);

public:
	device_t *m_netDevice;

private:
	net_device_t m_netDevices;
	bool m_isMultiQueryStart;
	byte m_recvBuf[512];
	bool m_initOk;

	void dump_buf(byte *cmd, int len);
	void initCmd(byte *cmd, int len, byte type);
	void doCrc(byte *cmd, int len);
	int32_t recv_resp(uint8_t *resp, int len);
};

#endif
