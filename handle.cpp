#include "monitor.h"
#include "handle.h"
#include "log.h"
#include "event.h"
#include "tagmatchwidget.h"
#include "os.h"
#include <qthread.h>

#include <qstringlist.h>

#pragma execution_character_set ("utf-8")

static QStringList g_tagList; 

static void formatTagStr(tag_t *tag, char *str, bool captal)
{
	if (captal)
		sprintf(str, "%02X%02X%02X%02X",
				tag->epc[0], tag->epc[1], tag->epc[2], tag->epc[3]);
	else
		sprintf(str, "%02x%02x%02x%02x",
				tag->epc[0], tag->epc[1], tag->epc[2], tag->epc[3]);
}

static void formatTagEventStr(tag_t *tag, char *tag_str, char *str)
{
	sprintf(str, "[%s] @ %d, RSSI = %3.2f", tag_str, tag->antanne_id, tag->rssi);
}

// 录制模式处理
static void handle_tag_shoot(tag_t *tag, monitor *m, tagFile *event)
{
	if (tag->antanne_id != m->m_shootAnt) {
		warLog(QString("当前天线不是录制天线(%1): %2！").arg(m->m_shootAnt).arg(tag->antanne_id));
		return;
	}

	char ev_str[64];
	char tag_str[16];

	logD("--> handle tag <--");

	formatTagStr(tag, tag_str, m->m_captal); 
	formatTagEventStr(tag, tag_str, ev_str);

	if (g_tagList.contains(tag_str)) {
		infoLog(QString("已经识别了 %1").arg(tag_str));
		return;
	}

	g_tagList << tag_str;
	infoLog(QString("{录制}检测到Tag: (%1)").arg(ev_str));

	event->addTagEvent(ev_str);
	m->addTagSum();

	m->setLedString(tag_str);
}

// 宰杀模式处理
static void handle_tag_kill(tag_t *tag, monitor *m)
{
#if 1
	if (tag->antanne_id != m->m_killAnt) {
		warLog(QString("当前天线不是宰杀天线(%1): %2！").arg(m->m_killAnt).arg(tag->antanne_id));
		return;
	}
#endif

	char str[64];
	char tag_str[16];

	formatTagStr(tag, tag_str, m->m_captal);

	if (g_tagList.contains(tag_str)) {
		infoLog(QString("已经识别了 %1").arg(tag_str));
		return;
	}
	g_tagList << tag_str;

	infoLog(QString("{宰杀}检测到Tag: [%1]").arg(tag_str));

	m->setLedString(tag_str);

	if (m->m_tagMatchWidget->matchTag(tag_str) == false) {
		m->showMatch(false);
		return;
	} else {
		m->showMatch(true);
	}
	warLog(QString("%1 在宰杀清单中").arg(tag_str));
	m->addTagSum();
	m->playSound();
}

void clearTagList()
{
	g_tagList.clear();
}

DWORD WINAPI readerThread(LPVOID param)
{
	byte old_epc[EPC_LEN] = { 0 };
	tag_t tag;
	monitor *m = (monitor *)param;
	RfReader *reader = m->m_reader;
	tagFile *event = m->m_tagEvent;

	if (reader->StartMultiQuery_v2() == false) {
		errLog("阅读器启动多次查询失败!");
		return -1;
	}

	okLog("阅读器开始检测...");
	logD("start reader");

	while (m->m_threadRunning) {
		logD("----------------- ReadMultiQuery_v2 ----------------------");
		bool ret = reader->ReadMultiQuery_v2(&tag);
		if (ret == false) { // no tag, continue
			logD("xxxxxxxxxxxxxxxxx no tag, continue xxxxxxxxxxxxxx");
			continue;
		}
		// same tag, continue;
		if (memcmp(tag.epc, old_epc, EPC_LEN) == 0) {
			logD("................. same tag, continue ...............");
			continue;
		}

		// stop query
		logD(">>>>>>>>>>>>>>>>> find tag! <<<<<<<<<<<<<<<<<<");
		reader->StopMultiQuery_v2();
		QThread::msleep(200); // To wait call of stop to receive its response data

		if (m->m_mode == mode_Shoot)
			handle_tag_shoot(&tag, m, event);
		else if (m->m_mode == mode_Kill)
			handle_tag_kill(&tag, m);
		else
			warLog("模式错误");
		memcpy(old_epc, tag.epc, EPC_LEN);

		// start query
		reader->StartMultiQuery_v2();
	}
	reader->StopMultiQuery_v2();

	okLog("阅读器停止检测.");
	return 0;
}
