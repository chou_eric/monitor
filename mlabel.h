#pragma once

#include <QtWidgets/QMainWindow>
#include "os.h"
#include <qlabel.h>
#include <qfont.h>
#include <qcolor.h>
#include "config.h"

class MLabel
{
public:
	MLabel(QLabel *label, Config *cfg, QString key);
	~MLabel();
	void chooseFont();
	void chooseColor();
	void setText(const QString &text);
	QString text() const;

private:
	Config *m_config;
	QLabel *m_label;
	QFont m_font;
	QColor m_color;
	QString m_fontKey;
	QString m_colorKey;

private:
	void setFont(QFont &f);
	void setColor(QColor &c);
};
